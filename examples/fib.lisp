(progn
  (defun fib (n) (progn
    (if (= n 0) (return 0))
    (setq a 0)
    (setq b 1)
    (setq c 0)
    (setq i 2)
    (loop
      (if (= i n) (return b))
      (setq i (1+ i))
      (setq c (+ a b))
      (setq a b)
      (setq b c))))
  (print (fib 69)))
