# Risp

![nails](nails.jpg)

A "simple" Lisp interpreter written in Rust, without dependencies.
Took inspiration from [here](https://vishpat.github.io/lisp-rs/overview.html)

## Supported features

### Arithmetic operations

+, -, \*, /, mod, rem, 1+, 1-

### Comparison operations

=, /=, >, <, >=, <=, max, min, char=, char/=, char>, char<, char>=, char<=

### Logical operations on boolean values

and, or, not

### Bitwise operations on numbers

logand, logior, lognot, logxor, lognand, lognor, logeq

### Character operations

char-code, code-char

### Hash table operations

make-hash-table, gethash, remhash, maphash

### Data types

nil, integer, float, boolean, char, string, list, cons, hash table, struct

### Decisions

cond, if, when, case

### Loops

loop, loop-while, loop-for, dotimes, dolist, do

### List functions

car, cdr, filter, map, reduce, length, append, reverse, nth, position

### Hash table functions

make-hash-table, gethash, remhash, maphash

### Predicates

null, atom, numberp, integerp, floatp, characterp, stringp, symbolp, functionp,
listp, consp, typep, zerop, evenp, oddp

### Other keywords

progn, let, setf, setq, defvar, defconstant, defun, defstruct, return, exit,
lambda, quote, type-of, format, print, read, eval, load
