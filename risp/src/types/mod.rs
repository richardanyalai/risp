mod char;
mod function;
mod value;

pub use crate::{
    eval::EvalError,
    types::{char::*, function::*, value::*},
};

pub type Int = i64;
pub type Float = f64;
pub type ValRes = Result<Value, EvalError>;

#[derive(Clone, Debug, derive_more::Display, PartialEq)]
pub enum Type {
    #[display(fmt = "NULL")]
    Null,

    #[display(fmt = "BOOLEAN")]
    Boolean,

    #[display(fmt = "NUMBER")]
    Number,

    #[display(fmt = "INTEGER")]
    Integer,

    #[display(fmt = "FLOAT")]
    Float,

    #[display(fmt = "CHARACTER")]
    Char,

    #[display(fmt = "STANDARD-CHAR")]
    StandardChar,

    #[display(fmt = "BASE-CHAR")]
    BaseChar,

    #[display(fmt = "STRING")]
    String,

    #[display(fmt = "SYMBOL")]
    Symbol,

    #[display(fmt = "SEQUENCE")]
    Sequence,

    #[display(fmt = "LIST")]
    List,

    #[display(fmt = "CONS")]
    Cons,

    #[display(fmt = "FUNCTION")]
    Function,

    #[display(fmt = "HASH-TABLE")]
    HashTable,

    Struct(String),
}
