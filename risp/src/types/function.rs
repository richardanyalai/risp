use std::{fmt, rc::Rc};

use crate::{env::Env, eval::EvalError, types::Value};

pub type NativeFn = fn(&[Value], &mut Env) -> Result<Value, EvalError>;

#[derive(Clone, Debug, PartialEq)]
pub struct Lambda {
    pub params: Vec<String>,
    pub body: Rc<Value>,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Function {
    Native(NativeFn),
    Lambda(Lambda),
}

impl fmt::Display for Function {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        match self {
            Function::Native(_) => write!(f, "#<FUNCTION>"),
            Function::Lambda(lambda) => write!(
                f,
                "#<FUNCTION :LAMBDA ({}) ({})>",
                lambda
                    .params
                    .iter()
                    .map(ToString::to_string)
                    .collect::<Vec<_>>()
                    .join(" "),
                lambda.body
            ),
        }
    }
}
