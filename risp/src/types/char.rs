#[derive(Clone, Copy, Debug, derive_more::Display, PartialEq)]
pub enum Char {
    Null,
    Backspace,
    Tab,
    Newline,
    Linefeed,
    Page,
    Return,
    Space,
    Rubout,
    Other(char),
}

impl Char {
    pub fn code(self) -> u8 {
        match self {
            Char::Null => 0,
            Char::Backspace => 8,
            Char::Tab => 9,
            Char::Newline | Char::Linefeed => 10,
            Char::Page => 12,
            Char::Return => 13,
            Char::Space => 32,
            Char::Rubout => 127,
            Char::Other(c) => c as u8,
        }
    }
}

impl From<u8> for Char {
    fn from(code: u8) -> Char {
        match code {
            0 => Char::Null,
            8 => Char::Backspace,
            9 => Char::Tab,
            10 => Char::Newline,
            12 => Char::Page,
            13 => Char::Return,
            32 => Char::Space,
            127 => Char::Rubout,
            _ => Char::Other(code as char),
        }
    }
}
