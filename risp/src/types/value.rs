use std::{collections::HashMap, fmt, rc::Rc};

use crate::{
    parser::{Literal, SExpression},
    types::*,
};

#[derive(Clone, Debug, derive_more::Display, PartialEq)]
pub enum Value {
    #[display(fmt = "NIL")]
    Nil,

    #[display(fmt = "T")]
    True,
    Integer(Int),
    Float(Float),
    Char(Char),

    #[display(fmt = "\"{_0}\"")]
    String(String),
    Symbol(String),
    List(List),
    Function(Function),

    #[display(fmt = "'{_0}")]
    Quoted(Rc<Value>),
    Debug(String),

    #[display(fmt = "#S(HASH-TABLE :TEST FASTHASH-EQL)")]
    HashTable(Vec<(Value, Value)>),

    #[display(fmt = "{}", "_0.to_uppercase()")]
    StructDef(String, Vec<String>),
    Struct(Struct),
    Return(Rc<Value>),
}

impl Value {
    pub fn type_of(&self) -> Type {
        match self {
            Value::Nil | Value::Quoted(_) => Type::Null,
            Value::True => Type::Boolean,
            Value::Integer(_) => Type::Integer,
            Value::Float(_) => Type::Float,
            Value::Char(c) => match c {
                Char::Other(_) => Type::StandardChar,
                _ => Type::BaseChar,
            },
            Value::String(_) => Type::String,
            Value::Symbol(_) | Value::Debug(_) => Type::Symbol,
            Value::List(_) => Type::Cons,
            Value::Function(_) => Type::Function,
            Value::HashTable(_) => Type::HashTable,
            Value::StructDef(name, _) => Type::Struct(name.to_uppercase()),
            Value::Struct(s) => Type::Struct(s.name.to_uppercase()),
            Value::Return(v) => v.type_of(),
        }
    }

    pub fn is_null(&self) -> bool {
        self.type_of() == Type::Null
    }
    pub fn is_number(&self) -> bool {
        self.type_of() == Type::Integer || self.type_of() == Type::Float
    }
    pub fn is_integer(&self) -> bool {
        self.type_of() == Type::Integer
    }
    pub fn is_float(&self) -> bool {
        self.type_of() == Type::Float
    }
    pub fn is_char(&self) -> bool {
        self.type_of() == Type::StandardChar || self.type_of() == Type::BaseChar
    }
    pub fn is_string(&self) -> bool {
        self.type_of() == Type::String
    }
    pub fn is_symbol(&self) -> bool {
        self.type_of() == Type::Symbol
    }
    pub fn is_cons(&self) -> bool {
        self.type_of() == Type::Cons
    }
    pub fn is_list(&self) -> bool {
        self.type_of() == Type::Cons
    }
    pub fn is_sequence(&self) -> bool {
        self.type_of() == Type::Cons || self.type_of() == Type::String
    }
    pub fn is_function(&self) -> bool {
        self.type_of() == Type::Function
    }
}

impl From<&SExpression> for Value {
    fn from(sexp: &SExpression) -> Value {
        match sexp {
            SExpression::Literal(l) => Value::from(l),
            SExpression::List(l) => Value::List(List(l.iter().map(Value::from).collect())),
        }
    }
}

impl From<&Literal> for Value {
    fn from(literal: &Literal) -> Value {
        match literal {
            Literal::Nil => Value::Nil,
            Literal::True => Value::True,
            Literal::Integer(i) => Value::Integer(*i),
            Literal::Float(f) => Value::Float(*f),
            Literal::Char(c) => Value::Char(*c),
            Literal::String(s) => Value::String(s.clone()),
            Literal::Symbol(s) => Value::Symbol(s.clone()),
            Literal::Quoted(q) => Value::Quoted(Rc::new(Value::from(q.as_ref()))),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct List(pub Vec<Value>);

impl fmt::Display for List {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        write!(
            f,
            "({})",
            self.0
                .iter()
                .map(ToString::to_string)
                .collect::<Vec<_>>()
                .join(" ")
        )
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Struct {
    pub name: String,
    pub values: HashMap<String, Value>,
}

impl fmt::Display for Struct {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        write!(
            f,
            "#S({} {})",
            self.name.to_uppercase(),
            self.values
                .keys()
                .map(|key| format!(":{key} {}", self.values.get(key).unwrap()))
                .collect::<Vec<_>>()
                .join(" ")
        )
    }
}
