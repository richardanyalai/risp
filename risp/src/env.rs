use std::collections::HashMap;

use crate::{
    functions::{
        arithmetic::*, bitwise::*, comparison::*, hash_table::*, lists::*, logical::*,
        predicates::*, *,
    },
    types::{Function, Value},
};

#[macro_export]
macro_rules! native_fn {
    ($value:expr) => {
        Value::Function(Function::Native($value))
    };
}

#[derive(Clone, Debug, PartialEq)]
pub enum EnvEntry {
    Variable(Value),
    Constant(Value),
}

impl EnvEntry {
    #[must_use]
    pub fn inner(&self) -> Value {
        match self {
            EnvEntry::Variable(v) | EnvEntry::Constant(v) => v.clone(),
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct Env<'a> {
    parent: Option<&'a Env<'a>>,
    vars: HashMap<String, EnvEntry>,
}

impl<'a> Env<'a> {
    fn new() -> Self {
        Env {
            vars: HashMap::new(),
            parent: None,
        }
    }

    pub fn update(&mut self, data: &'a Self) {
        self.vars
            .extend(data.vars.iter().map(|(k, v)| (k.clone(), v.clone())));
    }

    #[must_use]
    pub fn extend(parent: &'a Self) -> Env<'a> {
        Env {
            vars: HashMap::new(),
            parent: Some(parent),
        }
    }

    #[must_use]
    pub fn get(&self, name: &str) -> Option<EnvEntry> {
        match self.vars.get(name) {
            Some(var) => Some(var.clone()),
            None => self.parent.and_then(|parent| parent.get(name)),
        }
    }

    pub fn set(&mut self, name: &str, val: Value) -> Option<String> {
        if let Some(EnvEntry::Constant(_)) = self.get(name) {
            Some(format!(
                "{name} is a constant, may not be used as a variable"
            ))
        } else {
            self.vars.insert(name.to_string(), EnvEntry::Variable(val));
            None
        }
    }

    pub fn constant(&mut self, name: &str, val: Value) {
        self.vars.insert(name.to_string(), EnvEntry::Constant(val));
    }
}

impl<'a> Default for Env<'a> {
    fn default() -> Self {
        let mut env = Env::new();

        // Arithmetic operation

        env.set("+", native_fn!(builtin_add));
        env.set("-", native_fn!(builtin_sub));
        env.set("*", native_fn!(builtin_mul));
        env.set("/", native_fn!(builtin_div));
        env.set("mod", native_fn!(builtin_mod));
        env.set("rem", native_fn!(builtin_rem));
        env.set("1+", native_fn!(builtin_add_one));
        env.set("1-", native_fn!(builtin_sub_one));
        env.set("incf", native_fn!(builtin_incf));
        env.set("decf", native_fn!(builtin_decf));

        // Comparison operations

        env.set("=", native_fn!(builtin_eq));
        env.set("/=", native_fn!(builtin_ne));
        env.set(">", native_fn!(builtin_gt));
        env.set("<", native_fn!(builtin_lt));
        env.set(">=", native_fn!(builtin_ge));
        env.set("<=", native_fn!(builtin_le));
        env.set("max", native_fn!(builtin_max));
        env.set("min", native_fn!(builtin_min));
        env.set("char=", native_fn!(builtin_char_eq));
        env.set("char/=", native_fn!(builtin_char_ne));
        env.set("char>", native_fn!(builtin_char_gt));
        env.set("char<", native_fn!(builtin_char_lt));
        env.set("char>=", native_fn!(builtin_char_ge));
        env.set("char<=", native_fn!(builtin_char_le));
        env.set("string=", native_fn!(builtin_string_eq));
        env.set("string/=", native_fn!(builtin_string_ne));
        env.set("string>", native_fn!(builtin_string_gt));
        env.set("string<", native_fn!(builtin_string_lt));
        env.set("string>=", native_fn!(builtin_string_ge));
        env.set("string<=", native_fn!(builtin_string_le));

        // Logical operations

        env.set("and", native_fn!(builtin_and));
        env.set("or", native_fn!(builtin_or));
        env.set("not", native_fn!(builtin_not));

        // Bitwise operations

        env.set("logand", native_fn!(builtin_logand));
        env.set("logior", native_fn!(builtin_logior));
        env.set("lognot", native_fn!(builtin_lognot));
        env.set("logxor", native_fn!(builtin_logxor));
        env.set("lognand", native_fn!(builtin_lognand));
        env.set("lognor", native_fn!(builtin_lognor));
        env.set("logeqv", native_fn!(builtin_logeqv));
        env.set("ash", native_fn!(builtin_ash));

        // Predicates

        env.set("type-of", native_fn!(builtin_type_of));
        env.set("null", native_fn!(builtin_null));
        env.set("atom", native_fn!(builtin_atom));
        env.set("numberp", native_fn!(builtin_numberp));
        env.set("integerp", native_fn!(builtin_integerp));
        env.set("floatp", native_fn!(builtin_floatp));
        env.set("characterp", native_fn!(builtin_characterp));
        env.set("stringp", native_fn!(builtin_stringp));
        env.set("symbolp", native_fn!(builtin_symbolp));
        env.set("listp", native_fn!(builtin_listp));
        env.set("consp", native_fn!(builtin_consp));
        env.set("functionp", native_fn!(builtin_functionp));
        env.set("typep", native_fn!(builtin_typep));
        env.set("zerop", native_fn!(builtin_zerop));
        env.set("evenp", native_fn!(builtin_evenp));
        env.set("oddp", native_fn!(builtin_oddp));

        // Chars

        env.set("char-code", native_fn!(builtin_char_code));
        env.set("code-char", native_fn!(builtin_code_char));

        // Lists

        env.set("length", native_fn!(builtin_length));
        env.set("car", native_fn!(builtin_car));
        env.set("cdr", native_fn!(builtin_cdr));
        env.set("mapcar", native_fn!(builtin_mapcar));
        env.set("filter", native_fn!(builtin_filter));
        env.set("reduce", native_fn!(builtin_reduce));
        env.set("append", native_fn!(builtin_append));
        env.set("reverse", native_fn!(builtin_reverse));
        env.set("nth", native_fn!(builtin_nth));
        env.set("position", native_fn!(builtin_position));

        // Hash tables

        env.set("make-hash-table", native_fn!(builtin_make_hash_table));
        env.set("gethash", native_fn!(builtin_get_hash));
        env.set("remhash", native_fn!(builtin_rem_hash));
        env.set("maphash", native_fn!(builtin_map_hash));

        // Structs

        env.set("defstruct", native_fn!(builtin_defstruct));

        env
    }
}
