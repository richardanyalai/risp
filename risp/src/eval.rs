use crate::{
    env::Env,
    functions::{control_flow::*, lists::*, loops::*, *},
    parser::{parse, ParsingError, SExpression},
    types::{Function, List, Type, ValRes, Value},
};
use std::rc::Rc;

#[derive(Debug, thiserror::Error)]
pub enum EvalError {
    #[error("EVAL: unbound symbol {0}")]
    UnboundSymbol(String),

    #[error("EVAL: {0} is not a function")]
    NotAFunction(String),

    #[error("EVAL: invalid operation: ({0})")]
    InvalidOperation(String),

    #[error("EVAL: too few arguments given to {0}: {1}")]
    TooFewArguments(String, String),

    #[error("EVAL: too many arguments given to {0}: {1}")]
    TooManyArguments(String, String),

    #[error("EVAL: {0} is not a {1}")]
    WrongType(String, Type),

    #[error("EVAL: file path must be a string: {0}")]
    FilePathMustBeString(Value),

    #[error("EVAL: file \"{0}\" not found")]
    FileNotFound(String),

    #[error(transparent)]
    ParsingError(#[from] ParsingError),

    #[error(transparent)]
    TryFromIntError(#[from] std::num::TryFromIntError),

    #[error("EVAL: {0}")]
    IOError(#[from] std::io::Error),

    #[error("EVAL: {0}")]
    Other(String),
}

#[macro_export]
macro_rules! wrong_type {
    ($value:expr, $typ:expr) => {
        return Err($crate::eval::EvalError::WrongType($value, $typ))
    };
}

#[derive(Copy, Clone, PartialEq)]
pub enum ArgComp {
    Eq,
    Min,
    Max,
}

pub fn assert_args(
    function: &str,
    expected: usize,
    args: &[Value],
    comp: ArgComp,
) -> Result<(), EvalError> {
    let got = args.len();
    let fn_list = || {
        format!(
            "({function}{}{})",
            if got > 0 { " " } else { "" },
            args.iter()
                .map(ToString::to_string)
                .collect::<Vec<_>>()
                .join(" ")
        )
    };

    if got < expected && comp != ArgComp::Max {
        Err(EvalError::TooFewArguments(function.to_string(), fn_list()))
    } else if got > expected && comp != ArgComp::Min {
        Err(EvalError::TooManyArguments(function.to_string(), fn_list()))
    } else {
        Ok(())
    }
}

pub fn eval_function(name: &str, func: Function, args: &[Value], env: &mut Env) -> ValRes {
    env.set("$curr_func_name$", Value::String(name.to_lowercase()));

    match func {
        Function::Native(function) => function(args, env),
        Function::Lambda(lambda) => {
            assert_args("LAMBDA", lambda.params.len(), args, ArgComp::Eq)?;

            let mut env = Env::extend(env);

            for (name, arg) in std::iter::zip(lambda.params, args) {
                let mut default_env = Env::extend(&env);

                env.set(name.as_str(), eval_expression(arg, &mut default_env)?);
            }

            eval_expression(&lambda.body, &mut env)
        }
    }
}

pub fn eval_quoted(value: &Value, depth: usize) -> ValRes {
    let res = match value {
        Value::Quoted(q) => {
            let res = eval_quoted(q, depth + 1)?;

            if depth > 0 {
                Value::Quoted(Rc::new(q.as_ref().clone()))
            } else {
                res
            }
        }
        Value::List(List(l)) => {
            if l.len() == 2 && l[0] == Value::Symbol("quote".to_string()) {
                return Ok(Value::Quoted(Rc::new(l[1].clone())));
            }

            let l = l
                .iter()
                .map(|v| eval_quoted(v, depth + 1))
                .collect::<Result<Vec<_>, _>>()?;

            if depth > 1 {
                Value::Quoted(Rc::new(Value::List(List(l))))
            } else {
                Value::List(List(l))
            }
        }
        Value::Symbol(s) => {
            if depth > 2 {
                Value::Quoted(Rc::new(Value::Symbol(s.clone())))
            } else {
                Value::Debug(s.clone())
            }
        }
        _ => value.clone(),
    };

    Ok(res)
}

pub fn eval_expression(value: &Value, env: &mut Env) -> ValRes {
    match value {
        Value::Symbol(s) => Ok(env
            .get(s)
            .ok_or(EvalError::UnboundSymbol(s.clone()))?
            .inner()),
        Value::List(List(l)) => {
            if l.is_empty() {
                return Ok(Value::Nil);
            }

            let func = &l[0];
            let args = &l[1..].to_vec();

            if let Value::Symbol(func_name) = func {
                match func_name.to_lowercase().as_str() {
                    "exit" => return builtin_exit(args, env),
                    "return" => return builtin_return(args, env),
                    "write" | "print" => return builtin_print(args, env),
                    "format" => return builtin_format(args, env),
                    "quote" => return builtin_quote(args, env),
                    "list" => return builtin_list(args, env),
                    "cons" => return builtin_cons(args, env),
                    "progn" => return builtin_progn(args, env),
                    "let" => return builtin_let(args, env),
                    "if" | "when" => return builtin_if(args, env),
                    "cond" => return builtin_cond(args, env),
                    "case" => return builtin_case(args, env),
                    "loop" => return builtin_loop(args, env),
                    "dotimes" => return builtin_dotimes(args, env),
                    "dolist" => return builtin_dolist(args, env),
                    "do" => return builtin_do(args, env),
                    "defvar" | "setq" | "setf" => return builtin_setf(args, env),
                    "defconstant" => return builtin_defconstant(args, env),
                    "lambda" => return builtin_lambda(args),
                    "defun" => return builtin_defun(args, env),
                    "load" => return builtin_load(args, env),
                    "read" => return builtin_read(args, env),
                    "eval" => return builtin_eval(args, env),
                    _ => {}
                }
            };

            let func = eval_expression(func, env)?;

            match func {
                Value::Function(function) => eval_function(&l[0].to_string(), function, args, env),
                _ => Err(EvalError::NotAFunction(func.to_string())),
            }
        }
        Value::Quoted(_) => eval_quoted(value, 0),
        _ => Ok(value.clone()),
    }
}

pub fn eval_sexpression(sexp: &SExpression) -> ValRes {
    match sexp {
        SExpression::Literal(v) => Ok(Value::from(v)),
        SExpression::List(l) => {
            if l.is_empty() {
                return Ok(Value::Nil);
            }

            let list = l
                .iter()
                .map(eval_sexpression)
                .collect::<Result<Vec<_>, _>>()?;

            Ok(Value::List(List(list)))
        }
    }
}

pub fn eval(program: &str, env: &mut Env) -> ValRes {
    let parsed = parse(program)?;
    let value = eval_sexpression(&parsed)?;

    eval_expression(&value, env)
}

#[cfg(test)]
mod test {
    use std::{f64::consts::PI, rc::Rc};

    use crate::{eval::*, types::*};

    #[test]
    fn test_let() {
        let mut env = Env::default();
        let program = "
            (let ((x 2) (y 3))
                (let ((x 7) (z (+ x y)))
                (* z x))) 
        ";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(result, Value::Integer(35));
    }

    #[test]
    fn test_quote() {
        let mut env = Env::default();

        let result = eval("'69", &mut env).unwrap();
        assert_eq!(result, Value::Integer(69));

        let result = eval("(quote 69)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(69));

        let result = eval("'\"Hello, Friend!\"", &mut env).unwrap();
        assert_eq!(result, Value::String("Hello, Friend!".to_string()));

        let result = eval("(quote \"Hello, Friend!\")", &mut env).unwrap();
        assert_eq!(result, Value::String("Hello, Friend!".to_string()));

        let result = eval("'(1 2.5 '(3 \"4\"))", &mut env).unwrap();
        assert_eq!(
            result,
            Value::List(List(vec![
                Value::Integer(1),
                Value::Float(2.5),
                Value::Quoted(Rc::new(Value::List(List(vec![
                    Value::Integer(3),
                    Value::String("4".to_string())
                ]))))
            ]))
        );

        let result = eval("(quote (1 2.5 '(3 \"4\")))", &mut env).unwrap();
        assert_eq!(
            result,
            Value::List(List(vec![
                Value::Integer(1),
                Value::Float(2.5),
                Value::Quoted(Rc::new(Value::List(List(vec![
                    Value::Integer(3),
                    Value::String("4".to_string())
                ]))))
            ]))
        );

        let result = eval("'Heisenberg", &mut env).unwrap();
        assert_eq!(result, Value::Debug("Heisenberg".to_string()));

        let result = eval("(quote Heisenberg)", &mut env).unwrap();
        assert_eq!(result, Value::Debug("Heisenberg".to_string()));

        let result = eval("'(apple 'banana orange)", &mut env).unwrap();
        assert_eq!(
            result,
            Value::List(List(vec![
                Value::Debug("apple".to_string()),
                Value::Quoted(Rc::new(Value::Symbol("banana".to_string()))),
                Value::Debug("orange".to_string())
            ]))
        );

        let result = eval("'(apple (quote banana) orange)", &mut env).unwrap();
        assert_eq!(
            result,
            Value::List(List(vec![
                Value::Debug("apple".to_string()),
                Value::Quoted(Rc::new(Value::Symbol("banana".to_string()))),
                Value::Debug("orange".to_string())
            ]))
        );

        let result = eval("''a", &mut env).unwrap();
        assert_eq!(
            result,
            Value::Quoted(Rc::new(Value::Symbol("a".to_string())))
        );
    }

    #[test]
    fn test_eval() {
        let mut env = Env::default();

        let result = eval("(eval 1)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(1));

        let result = eval("(eval \"Evaluated string\")", &mut env).unwrap();
        assert_eq!(result, Value::String("Evaluated string".to_string()));

        let result = eval("(eval '(+ 1 2))", &mut env).unwrap();
        assert_eq!(result, Value::Integer(3));
    }

    // Types

    #[test]
    fn test_type_of() {
        let mut env = Env::default();

        let result = eval("(type-of nil)", &mut env).unwrap();
        assert_eq!(result, Value::Symbol(Type::Null.to_string()));

        let result = eval("(type-of t)", &mut env).unwrap();
        assert_eq!(result, Value::Symbol(Type::Boolean.to_string()));

        let result = eval("(type-of '())", &mut env).unwrap();
        assert_eq!(result, Value::Symbol(Type::Null.to_string()));

        let result = eval("(type-of ())", &mut env).unwrap();
        assert_eq!(result, Value::Symbol(Type::Null.to_string()));

        let result = eval("(type-of 69)", &mut env).unwrap();
        assert_eq!(result, Value::Symbol(Type::Integer.to_string()));

        let result = eval("(type-of 6.9)", &mut env).unwrap();
        assert_eq!(result, Value::Symbol(Type::Float.to_string()));

        let result = eval("(type-of #\\a)", &mut env).unwrap();
        assert_eq!(result, Value::Symbol(Type::StandardChar.to_string()));

        let result = eval("(type-of #\\Return)", &mut env).unwrap();
        assert_eq!(result, Value::Symbol(Type::BaseChar.to_string()));

        let result = eval("(type-of \"Hello, Friend\")", &mut env).unwrap();
        assert_eq!(result, Value::Symbol(Type::String.to_string()));

        let result = eval("(type-of 'a)", &mut env).unwrap();
        assert_eq!(result, Value::Symbol(Type::Symbol.to_string()));

        let result = eval("(type-of (lambda (n) (* n n)))", &mut env).unwrap();
        assert_eq!(result, Value::Symbol(Type::Function.to_string()));

        let result = eval("(type-of (type-of 6.9))", &mut env).unwrap();
        assert_eq!(result, Value::Symbol(Type::Symbol.to_string()));
    }

    #[test]
    fn test_type_predicates() {
        let mut env = Env::default();

        let result = eval("(atom 69)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(atom '(1 2 3))", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(numberp 69)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(integerp 69)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(floatp 69.420)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(characterp #\\Return)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(stringp \"Oh hi, Mark\")", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(symbolp 'im-a-symbol)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(consp '(1 3.5 \"4\"))", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(functionp (lambda (n) (1+ n)))", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(typep 23 'integer)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(typep #\\a 'character)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(typep #\\a 'standard-char)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(typep #\\a 'base-char)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);
    }

    #[test]
    fn test_null() {
        let mut env = Env::default();

        let result = eval("(null (list 1 2 3))", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(null \"lisp\")", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(null t)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(null 'symbol)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(null '())", &mut env).unwrap();
        assert_eq!(result, Value::True);
    }

    // Chars

    #[test]
    fn test_char_code() {
        let mut env = Env::default();

        let result = eval("(char-code #\\a)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(97));

        let result = eval("(char-code #\\A)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(65));

        let result = eval("(char-code #\\Null)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(0));

        let result = eval("(char-code #\\Space)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(32));
    }

    #[test]
    fn test_code_char() {
        let mut env = Env::default();

        let result = eval("(code-char 97)", &mut env).unwrap();
        assert_eq!(result, Value::Char(Char::Other('a')));

        let result = eval("(code-char 65)", &mut env).unwrap();
        assert_eq!(result, Value::Char(Char::Other('A')));

        let result = eval("(code-char 0)", &mut env).unwrap();
        assert_eq!(result, Value::Char(Char::Null));

        let result = eval("(code-char 32)", &mut env).unwrap();
        assert_eq!(result, Value::Char(Char::Space));
    }

    // Lists

    #[test]
    fn test_list() {
        let mut env = Env::default();

        let program = "(list 1 2.0 \"hello\")";
        let result = eval(program, &mut env).unwrap();

        assert_eq!(
            result,
            Value::List(List(vec![
                Value::Integer(1),
                Value::Float(2.0),
                Value::String("hello".to_string())
            ]))
        );

        let program = "'(1 2.0 \"hello\")";
        let result = eval(program, &mut env).unwrap();

        assert_eq!(
            result,
            Value::List(List(vec![
                Value::Integer(1),
                Value::Float(2.0),
                Value::String("hello".to_string())
            ]))
        );
    }

    #[test]
    fn test_cons() {
        let mut env = Env::default();
        let program = "(cons 1 (cons 2.0 (cons \"hello\" nil)))";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(
            result,
            Value::List(List(vec![
                Value::Integer(1),
                Value::Float(2.0),
                Value::String("hello".to_string())
            ]))
        );
    }

    #[test]
    fn test_car() {
        let mut env = Env::default();
        let result = eval("(car '(1 2 3))", &mut env).unwrap();

        assert_eq!(result, Value::Integer(1));
    }

    #[test]
    fn test_cdr() {
        let mut env = Env::default();
        let result = eval("(cdr '(1 2 3))", &mut env).unwrap();

        assert_eq!(
            result,
            Value::List(List(vec![Value::Integer(2), Value::Integer(3)]))
        );
    }

    #[test]
    fn test_length() {
        let mut env = Env::default();
        let result = eval("(length (list 1 2.3 \"lisp\"))", &mut env).unwrap();

        assert_eq!(result, Value::Integer(3));
    }

    #[test]
    fn test_append() {
        let mut env = Env::default();

        let result = eval("(append '(1 2 3) \"hello\")", &mut env).unwrap();
        assert_eq!(
            result,
            Value::List(List(vec![
                Value::Integer(1),
                Value::Integer(2),
                Value::Integer(3),
                Value::String("hello".to_string()),
            ]))
        );

        let result = eval("(append '(1 2 3) '(a b c))", &mut env).unwrap();
        assert_eq!(
            result,
            Value::List(List(vec![
                Value::Integer(1),
                Value::Integer(2),
                Value::Integer(3),
                Value::Debug("a".to_string()),
                Value::Debug("b".to_string()),
                Value::Debug("c".to_string())
            ]))
        );
    }

    #[test]
    fn test_reverse() {
        let mut env = Env::default();

        let result = eval("(reverse '(1 2 3))", &mut env).unwrap();
        assert_eq!(
            result,
            Value::List(List(vec![
                Value::Integer(3),
                Value::Integer(2),
                Value::Integer(1)
            ]))
        );

        let result = eval("(reverse \"I am the danger\")", &mut env).unwrap();
        assert_eq!(result, Value::String("regnad eht ma I".to_string()));
    }

    #[test]
    fn test_nth() {
        let mut env = Env::default();

        let result = eval("(nth 3 '(1 2 3 4))", &mut env).unwrap();
        assert_eq!(result, Value::Integer(4));

        let result = eval("(nth 4 '(1 2 3 4))", &mut env).unwrap();
        assert_eq!(result, Value::Nil);
    }

    #[test]
    fn test_position() {
        let mut env = Env::default();

        let result = eval("(position 3 '(1 2 3 4))", &mut env).unwrap();
        assert_eq!(result, Value::Integer(2));

        let result = eval("(position 5 '(1 2 3 4))", &mut env).unwrap();
        assert_eq!(result, Value::Nil);
    }

    // Map, filter, reduce

    #[test]
    fn test_map() {
        let mut env = Env::default();
        let program = "
          (progn
            (defvar sqr (lambda (n) (* n n)))
            (setq l (list 1 2 3 4 5))
            (mapcar sqr l))
        ";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(
            result,
            Value::List(List(vec![
                Value::Integer(1),
                Value::Integer(4),
                Value::Integer(9),
                Value::Integer(16),
                Value::Integer(25)
            ]))
        );
    }

    #[test]
    fn test_filter() {
        let mut env = Env::default();
        let program = "
          (progn
            (defvar odd (lambda (v) (= 1 (mod v 2))))
            (setq l (list 1 2 3 4 5))
            (filter odd l))
        ";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(
            result,
            Value::List(List(vec![
                Value::Integer(1),
                Value::Integer(3),
                Value::Integer(5)
            ]))
        );
    }

    #[test]
    fn test_reduce() {
        let mut env = Env::default();
        let program = "
          (progn
            (defvar odd (lambda (v) (= 1 (mod v 2))))
            (setq l (list 1 2 3 4 5))
            (reduce (lambda (x y) (or x y)) (mapcar odd l)))
        ";

        let result = eval(program, &mut env).unwrap();
        assert_eq!(result, Value::True);
    }

    // Structs

    #[test]
    fn test_struct() {
        let mut env = Env::default();

        let program = "
        (defstruct movie
          title
          release-date
          genre
          empty
          movie-id)
        ";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(
            result,
            Value::StructDef(
                "movie".to_string(),
                vec![
                    "title".to_string(),
                    "release-date".to_string(),
                    "genre".to_string(),
                    "empty".to_string(),
                    "movie-id".to_string()
                ]
            )
        );

        let program = "
        (setq movie1 (make-movie
          :title \"Fight Club\"
          :release-date 1999
          :genre \"action\"
          :movie-id 69420))
        ";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(
            result,
            Value::Struct(Struct {
                name: "movie".to_string(),
                values: vec![
                    ("title".to_string(), Value::String("Fight Club".to_string())),
                    ("release-date".to_string(), Value::Integer(1999)),
                    ("genre".to_string(), Value::String("action".to_string())),
                    ("empty".to_string(), Value::Nil),
                    ("movie-id".to_string(), Value::Integer(69420))
                ]
                .into_iter()
                .collect()
            })
        );

        let program = "(movie-release-date movie1)";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(result, Value::Integer(1999));

        let program = "(setq movie2 (copy-movie movie1))";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(
            result,
            Value::Struct(Struct {
                name: "movie".to_string(),
                values: vec![
                    ("title".to_string(), Value::String("Fight Club".to_string())),
                    ("release-date".to_string(), Value::Integer(1999)),
                    ("genre".to_string(), Value::String("action".to_string())),
                    ("empty".to_string(), Value::Nil),
                    ("movie-id".to_string(), Value::Integer(69420))
                ]
                .into_iter()
                .collect()
            })
        );

        let program = "(setq (movie-genre movie2) \"thriller\")";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(result, Value::String("thriller".to_string()));

        let program = "(movie-genre movie2)";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(result, Value::String("thriller".to_string()));
    }

    // Arithmetic operations

    #[test]
    fn test_add() {
        let mut env = Env::default();

        let result = eval("(+ 1 2)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(3));

        let result = eval("(+ 1.0 2 4)", &mut env).unwrap();
        assert_eq!(result, Value::Float(7.0));

        let result = eval("(+ 1.0 2.0)", &mut env).unwrap();
        assert_eq!(result, Value::Float(3.0));

        let result = eval("(+ 1)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(1));
    }

    #[test]
    fn test_sub() {
        let mut env = Env::default();

        let result = eval("(- 1.0 2)", &mut env).unwrap();
        assert_eq!(result, Value::Float(-1.0));

        let result = eval("(- 7 4 3)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(0));

        let result = eval("(- 1)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(-1));
    }

    #[test]
    fn test_mul() {
        let mut env = Env::default();

        let result = eval("(* 1.0 2)", &mut env).unwrap();
        assert_eq!(result, Value::Float(2.0));

        let result = eval("(* 1 2 3)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(6));

        let result = eval("(* 1)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(1));
    }

    #[test]
    fn test_div() {
        let mut env = Env::default();

        let result = eval("(/ 1.0 2)", &mut env).unwrap();
        assert_eq!(result, Value::Float(0.5));

        let result = eval("(/ 4.0 2 5)", &mut env).unwrap();
        assert_eq!(result, Value::Float(0.4));

        let result = eval("(/ 1)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(1));
    }

    #[test]
    fn test_modulo() {
        let mut env = Env::default();

        let result = eval("(mod 21.0 20.0)", &mut env).unwrap();
        assert_eq!(result, Value::Float(1.0));
    }

    #[test]
    fn test_remainder() {
        let mut env = Env::default();

        let result = eval("(rem -1 5)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(-1));
    }

    #[test]
    fn test_add_one() {
        let mut env = Env::default();

        let result = eval("(1+ 1)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(2));

        let result = eval("(1+ 1.3)", &mut env).unwrap();
        assert_eq!(result, Value::Float(2.3));
    }

    #[test]
    fn test_sub_one() {
        let mut env = Env::default();

        let result = eval("(1- 2)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(1));

        let result = eval("(1- 2.3)", &mut env).unwrap();
        assert_eq!(result, Value::Float(1.3));
    }

    // Bitwise operations on numbers

    #[test]
    fn test_lognot() {
        let mut env = Env::default();

        let result = eval("(lognot 69)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(-70));
    }

    #[test]
    fn test_logand() {
        let mut env = Env::default();

        let result = eval("(logand)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(-1));

        let result = eval("(logand 69 420)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(4));

        let result = eval("(logand 690 420 1999)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(128));
    }

    #[test]
    fn test_logior() {
        let mut env = Env::default();

        let result = eval("(logior)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(0));

        let result = eval("(logior 69 420)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(485));

        let result = eval("(logior 690 420 1999)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(2047));
    }

    #[test]
    fn test_logxor() {
        let mut env = Env::default();

        let result = eval("(logxor)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(0));

        let result = eval("(logxor 69 420)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(481));

        let result = eval("(logxor 690 420 1999)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(1241));
    }

    #[test]
    fn test_lognand() {
        let mut env = Env::default();

        let result = eval("(lognand)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(-1));

        let result = eval("(lognand 69 420)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(-5));
    }

    #[test]
    fn test_lognor() {
        let mut env = Env::default();

        let result = eval("(lognor)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(-1));

        let result = eval("(lognor 69 420)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(-486));
    }

    #[test]
    fn test_logeqv() {
        let mut env = Env::default();

        let result = eval("(logeqv)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(-1));

        let result = eval("(logeqv 69 420)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(-482));

        let result = eval("(logeqv 690 420 1999)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(1241));
    }

    #[test]
    fn test_ash() {
        let mut env = Env::default();

        let result = eval("(ash #b111111 1)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(0b011111));
    }

    // Comparison operations

    #[test]
    fn test_eq() {
        let mut env = Env::default();

        let result = eval("(= 69.0 69)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(= 69.1 69)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(= 69 69)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(= 69 69 69)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(= 69 420)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);
    }

    #[test]
    fn test_not_eq() {
        let mut env = Env::default();

        let result = eval("(/= 69.0 69)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(/= 69.1 69)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(/= 69 69)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(/= 69 420)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(/= 0 69 420)", &mut env).unwrap();
        assert_eq!(result, Value::True);
    }

    #[test]
    fn test_greater_than() {
        let mut env = Env::default();

        let result = eval("(> 420 69)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(> 1 2)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(> 1.1 1)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(> 1.0 1)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);
    }

    #[test]
    fn test_less_than() {
        let mut env = Env::default();

        let result = eval("(< 69 420)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(< 2 1)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(< 1 1.1)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(< 1 1.0)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);
    }

    #[test]
    fn test_greater_than_or_equal_to() {
        let mut env = Env::default();

        let result = eval("(>= 420 69)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(>= 420.0 420)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(>= 69 420)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);
    }

    #[test]
    fn test_less_than_or_equal_to() {
        let mut env = Env::default();

        let result = eval("(<= 69 420)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(<= 420.0 420)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(<= 420 69)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);
    }

    #[test]
    fn test_max() {
        let mut env = Env::default();

        let result = eval("(max 69.1 1999 420)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(1999));
    }

    #[test]
    fn test_min() {
        let mut env = Env::default();

        let result = eval("(min 69.1 1999 420)", &mut env).unwrap();
        assert_eq!(result, Value::Float(69.1));
    }

    #[test]
    fn test_char_comp() {
        let mut env = Env::default();

        let result = eval("(char= #\\a)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(char= #\\a #\\a)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(char= #\\a #\\b)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(char/= #\\a)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(char/= #\\a #\\a)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(char/= #\\a #\\b)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(char> #\\c #\\a)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(char> #\\a #\\c)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(char> #\\a #\\a)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(char< #\\e #\\g)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(char< #\\g #\\e)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(char< #\\a #\\a)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(char>= #\\c #\\a)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(char>= #\\a #\\c)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(char>= #\\a #\\a)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(char<= #\\e #\\g)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(char<= #\\g #\\e)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(char<= #\\a #\\a)", &mut env).unwrap();
        assert_eq!(result, Value::True);
    }

    #[test]
    fn test_string_comp() {
        let mut env = Env::default();

        let result = eval("(string= \"Daryl\")", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(string= \"Rick\" \"Rick\")", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(string/= \"Michonne\")", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(string/= \"Carl\" \"Carl\")", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(string/= \"one\" \"two\")", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(string> \"Walter\" \"Elliot\")", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(string> \"Gale\" \"Jesse\")", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(string> \"Gus\" \"Gus\")", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(string< \"Gale\" \"Jesse\")", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(string< \"Walter\" \"Jesse\")", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(string< \"Jesse\" \"Jesse\")", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(string>= \"Mike\" \"Gus\")", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(string>= \"Mike\" \"Walter\")", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(string>= \"Jane\" \"Jane\")", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(string<= \"Jesse\" \"Mike\")", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(string<= \"Saul\" \"Flynn\")", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(string<= \"Hank\" \"Hank\")", &mut env).unwrap();
        assert_eq!(result, Value::True);
    }

    // Logical operations on boolean values

    #[test]
    fn test_not() {
        let mut env = Env::default();

        let result = eval("(not nil)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(not t)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(not 6)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(not '(1 4 5.7))", &mut env).unwrap();
        assert_eq!(result, Value::Nil);
    }

    #[test]
    fn test_and() {
        let mut env = Env::default();

        let result = eval("(and nil 2 3)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(and t 2 3.0)", &mut env).unwrap();
        assert_eq!(result, Value::Float(3.0));

        let result = eval("(and 6 t 8 \"Hola\")", &mut env).unwrap();
        assert_eq!(result, Value::String("Hola".to_string()));

        let result = eval("(and '(1 4 5.7) 2.4 1)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(1));
    }

    #[test]
    fn test_or() {
        let mut env = Env::default();

        let result = eval("(or nil 2 3)", &mut env).unwrap();
        assert_eq!(result, Value::Integer(2));

        let result = eval("(or t 2 3.0)", &mut env).unwrap();
        assert_eq!(result, Value::True);

        let result = eval("(or nil nil)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(or nil '(1 4 5.7) 2.4 1)", &mut env).unwrap();
        assert_eq!(
            result,
            Value::List(List(vec![
                Value::Integer(1),
                Value::Integer(4),
                Value::Float(5.7),
            ]))
        );
    }

    // Decisions

    #[test]
    fn test_if() {
        let mut env = Env::default();
        let program = "
          (progn
            (setq year 1999)
            (if (= year 1999) 1 0))
        ";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(result, Value::Integer(1));
    }

    #[test]
    fn test_when() {
        let mut env = Env::default();
        let program = "
          (progn
            (setq year 1999)
            (when (= year 1999) 1))
        ";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(result, Value::Integer(1));
    }

    #[test]
    fn test_cond() {
        let mut env = Env::default();
        let program = "
          (progn
            (setq a 10)
            (cond 
              ((> a 20) \"no way\")
              ((> a 5) \"definitely\")))
        ";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(result, Value::String("definitely".to_string()));
    }

    #[test]
    fn test_case() {
        let mut env = Env::default();
        let program = "
          (progn
            (setq day 4)
            (case day
              (1 (print \"Monday\"))
              (2 (print \"Tuesday\"))
              (3 (print \"Wednesday\"))
              (4 (print \"Thursday\"))
              (5 (print \"Friday\"))
              (6 (print \"Saturday\"))
              (7 (print \"Sunday\"))))
        ";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(result, Value::Integer(4));
    }

    // Loops

    #[test]
    fn test_loop() {
        let mut env = Env::default();
        let program = "
          (progn
            (setq a 10)
            (loop 
              (incf a)
              (print a)
              (if (> a 22) (return a))))
        ";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(result, Value::Integer(23));
    }

    #[test]
    fn test_loop_while() {
        let mut env = Env::default();

        let program = "
          (progn
            (setq i 10)
            (loop while (> i 0) do (decf i)))
        ";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let program = "
          (progn
            (setq i 10)
            (loop while (> i 0) do (if (< i 5) (return i)) (decf i)))
        ";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(result, Value::Integer(4));
    }

    #[test]
    fn test_loop_for() {
        let mut env = Env::default();

        let program = "(loop for i from 0 to 10)";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let program = "
          (loop for i from 1 to 5 do
            (if (> i 3) (return i)))
        ";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(result, Value::Integer(4));

        let program = "(loop for i in '(2 4 6 8 10))";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let program = "
          (loop for i in '(2 4 6 8 10) do
            (if (> i 5) (return i)))
        ";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(result, Value::Integer(6));
    }

    #[test]
    fn test_dotimes() {
        let mut env = Env::default();
        let program = "
          (dotimes (n 420)
            (print n)
            (if (> n 68) (return n)))
        ";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(result, Value::Integer(69));
    }

    #[test]
    fn test_dolist() {
        let mut env = Env::default();
        let program = "
          (dolist (n '(1 2 3 4 5 6 7 8 9))
            (print n)
            (if (> n 6) (return n)))
        ";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(result, Value::Integer(7));
    }

    #[test]
    fn test_do() {
        let mut env = Env::default();
        let program = "
          (do ((x 0 (+ 2 x))
            (y 20 ( - y 2)))
            ((= x y)(- x y))
            (print y))
        ";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(result, Value::Integer(0));
    }

    // Hash tables

    #[test]
    fn test_hash_tables() {
        let mut env = Env::default();

        let program = "(setq htable (make-hash-table))";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(result, Value::HashTable(vec![]));

        let program = "(gethash 'empty htable)";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let program = "
          (progn
            (setq (gethash 'empty htable) 69)
            (gethash 'empty htable))
        ";
        let result = eval(program, &mut env).unwrap();
        assert_eq!(result, Value::Integer(69));
    }

    // Definitions of variables, constants, lambdas etc

    #[test]
    fn test_defvar() {
        let mut env = Env::default();
        let program = "
          (progn
            (defvar age 69)
            age)";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(result, Value::Integer(69));
    }

    #[test]
    #[should_panic]
    fn test_defconstant() {
        let mut env = Env::default();
        let program = "
          (progn
            (defconstant age 69)
            (defvar age 420))";

        eval(program, &mut env).unwrap();
    }

    #[test]
    fn test_inline_lambda() {
        let mut env = Env::default();
        let program = "((lambda (a b c) (+ a (- b c))) 4 5 6)";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(result, Value::Integer(3));
    }

    #[test]
    fn test_area_of_a_circle() {
        let mut env = Env::default();
        let program = format!(
            "
              (progn
                (let ((r 5.0) (pi {PI}))
                (* pi (* r r))))
            "
        );

        let result = eval(&program, &mut env).unwrap();

        assert_eq!(result, Value::Float(PI * 5.0 * 5.0));
    }

    #[test]
    fn test_sqr_lambda() {
        let mut env = Env::default();
        let program = "
          (progn
            (defvar sqr (lambda (n) (* n n))) 
            (sqr 10))
        ";

        let result = eval(program, &mut env).unwrap();
        assert_eq!(result, Value::Integer((10 * 10) as Int));
    }

    #[test]
    fn test_fibonaci_function() {
        let mut env = Env::default();
        let program = "
          (progn
            (defun fib (n) (if (< n 2) 1 (+ (fib (- n 1)) (fib (- n 2)))))
            (fib 10))
        ";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(result, Value::Integer(89));
    }

    #[test]
    fn test_factorial_function() {
        let mut env = Env::default();
        let program = "
          (progn
            (defun fact (n) (if (< n 1) 1 (* n (fact (- n 1)))))
            (fact 5))
        ";

        let result = eval(program, &mut env).unwrap();

        assert_eq!(result, Value::Integer(120));
    }

    #[test]
    fn test_circle_area_lambda() {
        let mut env = Env::default();
        let program = format!(
            "
              (progn
                (setq pi {PI})
                (setq r 10)
                (defvar sqr (lambda (n) (* n n)))
                (defvar area (lambda (r) (* pi (sqr r))))
                (area r))
            "
        );

        let result = eval(&program, &mut env).unwrap();

        assert_eq!(result, Value::Float(PI * 10.0 * 10.0));
    }

    // Misc

    #[test]
    fn test_format() {
        let mut env = Env::default();

        let result = eval("(format t \"~a = 69\" 69)", &mut env).unwrap();
        assert_eq!(result, Value::Nil);

        let result = eval("(format nil \"~a = 69\" 69)", &mut env).unwrap();
        assert_eq!(result, Value::String("69 = 69".to_string()));

        let result = eval("(format nil \"~b\" 69)", &mut env).unwrap();
        assert_eq!(result, Value::String("1000101".to_string()));

        let result = eval("(format nil \"~o\" 69)", &mut env).unwrap();
        assert_eq!(result, Value::String("105".to_string()));

        let result = eval("(format nil \"~d\" 69)", &mut env).unwrap();
        assert_eq!(result, Value::String("69".to_string()));

        let result = eval("(format nil \"~x\" 69)", &mut env).unwrap();
        assert_eq!(result, Value::String("45".to_string()));
    }

    #[test]
    fn test_load_file() {
        let mut env = Env::default();

        let result = eval("(load \"../examples/fib.lisp\")", &mut env).unwrap();

        assert_eq!(result, Value::Integer(72723460248141));
    }
}
