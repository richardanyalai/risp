use std::rc::Rc;

use crate::{
    env::Env,
    eval::{assert_args, eval_expression, ArgComp, EvalError},
    types::{List, ValRes, Value},
};

pub fn builtin_return(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("RETURN", 1, args, ArgComp::Eq)?;

    Ok(Value::Return(Rc::new(eval_expression(&args[0], env)?)))
}

pub fn builtin_if(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("IF", 2, args, ArgComp::Min)?;
    assert_args("IF", 3, args, ArgComp::Max)?;

    match eval_expression(&args[0], env)? {
        Value::Nil => {
            if args.len() == 2 {
                return Ok(Value::Nil);
            }

            eval_expression(&args[2], env)
        }
        _ => eval_expression(&args[1], env),
    }
}

pub fn builtin_cond(args: &[Value], env: &mut Env) -> ValRes {
    let mut res = Value::Nil;

    for branch in args {
        match branch {
            Value::List(List(l)) => {
                assert_args("COND", 2, args, ArgComp::Min)?;

                if eval_expression(&l[0], env)? != Value::Nil {
                    res = eval_expression(&l[1], env)?;
                }

                if res != Value::Nil {
                    return Ok(res);
                }
            }
            _ => return Err(EvalError::Other("Invalid COND branch".to_string())),
        }
    }

    Ok(res)
}

pub fn builtin_case(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("CASE", 2, args, ArgComp::Min)?;

    match eval_expression(&args[0], env) {
        Ok(value) => {
            for branch in &args[1..] {
                match branch {
                    Value::List(List(l)) if l.len() == 2 => {
                        if value == l[0] {
                            eval_expression(&l[1], env)?;
                            return Ok(l[0].clone());
                        }
                    }
                    _ => return Err(EvalError::Other("Invalid CASE branch".to_string())),
                }
            }

            Ok(Value::Nil)
        }
        _ => Err(EvalError::Other("Condition must be a boolean".to_string())),
    }
}
