use crate::{
    env::Env,
    eval::{assert_args, eval_expression, ArgComp, EvalError},
    types::{Type, ValRes, Value},
    wrong_type,
};

macro_rules! bool {
    ($cond:expr) => {
        if $cond {
            Value::True
        } else {
            Value::Nil
        }
    };
}

pub fn builtin_not(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("NOT", 1, args, ArgComp::Eq)?;

    Ok(bool!(eval_expression(&args[0], env)?.is_null()))
}

pub fn builtin_type_of(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("TYPE-OF", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    Ok(Value::Symbol(arg.type_of().to_string()))
}

pub fn builtin_null(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("NULL", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    Ok(bool!(arg.is_null()))
}

pub fn builtin_atom(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("ATOM", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    Ok(bool!(!arg.is_cons()))
}

pub fn builtin_numberp(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("NUMBERP", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    Ok(bool!(arg.is_number()))
}

pub fn builtin_integerp(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("INTEGERP", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    Ok(bool!(arg.is_integer()))
}

pub fn builtin_floatp(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("FLOATP", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    Ok(bool!(arg.is_float()))
}

pub fn builtin_characterp(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("CHARACTERP", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    Ok(bool!(arg.is_char()))
}

pub fn builtin_stringp(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("STRINGP", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    Ok(bool!(arg.is_string()))
}

pub fn builtin_symbolp(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("SYMBOLP", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    Ok(bool!(arg.is_symbol()))
}

pub fn builtin_listp(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("LISTP", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    Ok(bool!(arg.is_list()))
}

pub fn builtin_consp(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("CONSP", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    Ok(bool!(arg.is_cons()))
}

pub fn builtin_functionp(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("FUNCTIONP", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    Ok(bool!(arg.is_function()))
}

pub fn builtin_typep(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("TYPEP", 2, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;
    let typ = eval_expression(&args[1], env)?;

    if let Value::Debug(typ) = typ {
        let typ = typ.to_uppercase();

        let res = arg.type_of().to_string() == typ
            || typ == Type::Number.to_string() && arg.is_number()
            || typ == Type::Char.to_string() && arg.is_char()
            || typ == Type::Sequence.to_string() && arg.is_sequence();

        Ok(bool!(res))
    } else {
        Err(EvalError::Other(format!(
            "invalid type specification {typ}"
        )))
    }
}

pub fn builtin_zerop(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("ZEROP", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    match arg {
        Value::Integer(i) => Ok(bool!(i == 0)),
        Value::Float(f) => Ok(bool!(f == 0.0)),
        _ => wrong_type!(arg.to_string(), Type::Integer),
    }
}

pub fn builtin_evenp(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("EVENP", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    match arg {
        Value::Integer(i) => Ok(bool!(i % 2 == 0)),
        _ => wrong_type!(arg.to_string(), Type::Integer),
    }
}

pub fn builtin_oddp(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("ODDP", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    match arg {
        Value::Integer(i) => Ok(bool!(i % 2 != 0)),
        _ => wrong_type!(arg.to_string(), Type::Integer),
    }
}
