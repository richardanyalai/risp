use crate::{
    env::Env,
    eval::eval_expression,
    types::{ValRes, Value},
};

pub fn builtin_and(args: &[Value], env: &mut Env) -> ValRes {
    let mut curr = Value::Nil;

    for value in args {
        let value = eval_expression(value, env)?;

        if value == Value::Nil {
            return Ok(Value::Nil);
        }

        curr = value.clone();
    }

    Ok(curr)
}

pub fn builtin_or(args: &[Value], env: &mut Env) -> ValRes {
    for item in args {
        let value = eval_expression(item, env)?;

        match value {
            Value::Nil | Value::Function(_) => {}
            _ => return Ok(value),
        }
    }

    Ok(Value::Nil)
}
