use crate::{
    env::Env,
    eval::{assert_args, eval_expression, ArgComp, EvalError},
    types::{Function, Int, Lambda, List, Type, ValRes, Value},
    wrong_type,
};

pub fn builtin_list(args: &[Value], env: &mut Env) -> ValRes {
    let res = if args.is_empty() {
        Value::Nil
    } else {
        Value::List(List(
            args.iter()
                .map(|value| eval_expression(value, env))
                .collect::<Result<Vec<_>, _>>()?,
        ))
    };

    Ok(res)
}

pub fn builtin_cons(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("CONS", 2, args, ArgComp::Eq)?;

    match args {
        [car, cdr] => eval_expression(cdr, env).and_then(|t| match t {
            Value::Nil => Ok(Value::List(List(vec![car.clone()]))),
            Value::List(List(mut l)) => {
                l.insert(0, car.clone());
                Ok(Value::List(List(l)))
            }
            _ => Err(EvalError::Other("Cons list missing NIL".to_string())),
        }),
        _ => Err(EvalError::Other("Invalid cons list".to_string())),
    }
}

pub fn builtin_car(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("CAR", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    match arg {
        Value::List(List(list)) => Ok(list[0].clone()),
        _ => wrong_type!(arg.to_string(), Type::List),
    }
}

pub fn builtin_cdr(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("CDR", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    match arg {
        Value::List(List(list)) => Ok(Value::List(List(list[1..].to_vec()))),
        _ => wrong_type!(arg.to_string(), Type::List),
    }
}

pub fn builtin_length(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("LENGTH", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    match arg {
        Value::List(List(list)) => Ok(Value::Integer(list.len() as Int)),
        Value::String(string) => Ok(Value::Integer(string.len() as Int)),
        Value::Nil => Ok(Value::Integer(0)),
        _ => wrong_type!(arg.to_string(), Type::Sequence),
    }
}

pub fn builtin_mapcar(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("MAPCAR", 2, args, ArgComp::Eq)?;

    let lambda = eval_expression(&args[0], env)?;
    let arg_list = eval_expression(&args[1], env)?;

    let (params, body) = match &lambda {
        Value::Function(Function::Lambda(Lambda { params, body })) => {
            assert_args("MAPCAR lambda", 1, args, ArgComp::Min)?;

            (params, body)
        }
        _ => {
            return Err(EvalError::Other(format!(
                "No lambda while evaluating MAPCAR: {lambda}"
            )))
        }
    };

    let Value::List(List(args)) = arg_list else {
        return Err(EvalError::Other(format!(
            "Invalid MAP arguments: {arg_list}"
        )));
    };

    let func_param = &params[0];
    let result_list = args
        .iter()
        .map(|arg| {
            let val = eval_expression(arg, env)?;
            let mut new_env = Env::extend(env);

            new_env.set(func_param, val);

            eval_expression(body, &mut new_env)
        })
        .collect::<Result<Vec<_>, _>>()?;

    Ok(Value::List(List(result_list)))
}

pub fn builtin_filter(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("FILTER", 2, args, ArgComp::Eq)?;

    let lambda = eval_expression(&args[0], env)?;
    let arg_list = eval_expression(&args[1], env)?;

    let (params, body) = match &lambda {
        Value::Function(Function::Lambda(Lambda { params, body })) => {
            assert_args("FILTER lambda", 1, args, ArgComp::Min)?;

            (params, body)
        }
        _ => {
            return Err(EvalError::Other(format!(
                "No lambda while evaluating FILTER: {lambda}"
            )))
        }
    };

    let Value::List(List(args)) = arg_list else {
        return Err(EvalError::Other(format!(
            "Invalid FILTER arguments: {arg_list}",
        )));
    };

    let func_param = &params[0];
    let mut result_list = Vec::new();

    for arg in &args {
        let val = eval_expression(arg, env)?;
        let mut new_env = Env::extend(env);

        new_env.set(func_param, val.clone());

        let value = eval_expression(body, &mut new_env)?;
        let result = match value {
            Value::True => true,
            Value::Nil => false,
            _ => return Err(EvalError::Other(format!("Invalid FILTER result: {value}",))),
        };

        if result {
            result_list.push(val);
        }
    }

    Ok(Value::List(List(result_list)))
}

pub fn builtin_reduce(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("REDUCE", 2, args, ArgComp::Eq)?;

    let lambda = eval_expression(&args[0], env)?;
    let arg_list = eval_expression(&args[1], env)?;

    let (params, body) = match &lambda {
        Value::Function(Function::Lambda(Lambda { params, body })) => {
            assert_args("REDUCE lambda", 2, args, ArgComp::Eq)?;

            (params, body)
        }
        _ => {
            return Err(EvalError::Other(format!(
                "No lambda while evaluating REDUCE: {lambda}"
            )))
        }
    };

    let Value::List(List(args)) = arg_list else {
        return Err(EvalError::Other(format!(
            "Invalid REDUCE arguments: {arg_list}"
        )));
    };

    assert_args("REDUCE", 2, &args, ArgComp::Min)?;

    let reduce_param1 = &params[0];
    let reduce_param2 = &params[1];
    let mut accumulator = eval_expression(&args[0], env)?;

    for arg in &args[1..] {
        let mut new_env = Env::extend(env);

        new_env.set(reduce_param1, accumulator.clone());

        let val = eval_expression(arg, &mut new_env)?;

        new_env.set(reduce_param2, val);

        accumulator = eval_expression(body, &mut new_env)?;
    }

    Ok(accumulator)
}

pub fn builtin_append(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("APPEND", 2, args, ArgComp::Eq)?;

    let Value::List(List(mut list)) = eval_expression(&args[0], env)? else {
        wrong_type!(args[0].to_string(), Type::List);
    };

    let item = eval_expression(&args[1], env)?;

    match item {
        Value::List(List(mut l)) => list.append(&mut l),
        _ => list.push(item),
    }

    Ok(Value::List(List(list)))
}

pub fn builtin_reverse(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("REVERSE", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    match arg {
        Value::List(List(l)) => Ok(Value::List(List(l.iter().cloned().rev().collect()))),
        Value::String(s) => Ok(Value::String(s.chars().rev().collect())),
        _ => wrong_type!(arg.to_string(), Type::Sequence),
    }
}

pub fn builtin_nth(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("NTH", 2, args, ArgComp::Eq)?;

    let index = eval_expression(&args[0], env)?;

    let Value::Integer(index) = index else {
        wrong_type!(index.to_string(), Type::Integer)
    };

    let list = eval_expression(&args[1], env)?;

    let Value::List(List(list)) = list else {
        wrong_type!(list.to_string(), Type::List)
    };

    Ok(list
        .get(usize::try_from(index).expect("Index shouldn't be out of bounds"))
        .unwrap_or(&Value::Nil)
        .clone())
}

pub fn builtin_position(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("POSITION", 2, args, ArgComp::Eq)?;

    let value = eval_expression(&args[0], env)?;
    let seq = eval_expression(&args[1], env)?;

    let res = match &seq {
        Value::List(List(l)) => l
            .iter()
            .enumerate()
            .find(|item| *item.1 == value)
            .map_or(Value::Nil, |item| Value::Integer(item.0 as Int)),
        Value::String(s) => s
            .chars()
            .enumerate()
            .find(|ch| {
                if let Value::Char(c) = value {
                    ch.1 as u8 == c.code()
                } else {
                    false
                }
            })
            .map_or(Value::Nil, |ch| Value::Integer(ch.0 as Int)),
        _ => wrong_type!(seq.to_string(), Type::Sequence),
    };

    Ok(res)
}
