pub mod arithmetic;
pub mod bitwise;
pub mod comparison;
pub mod control_flow;
pub mod hash_table;
pub mod lists;
pub mod logical;
pub mod loops;
pub mod predicates;

use std::{collections::HashMap, fs, rc::Rc};

use crate::{
    env::Env,
    eval::{
        assert_args, eval, eval_expression, eval_function, eval_sexpression, ArgComp, EvalError,
    },
    functions::hash_table::builtin_set_hash,
    native_fn,
    parser::parse,
    types::{Char, Function, Int, Lambda, List, Struct, Type, ValRes, Value},
    wrong_type,
};

pub fn builtin_exit(args: &[Value], _env: &mut Env) -> ValRes {
    let mut exit_code = 0;

    if !args.is_empty() {
        assert_args("EXIT", 1, args, ArgComp::Max)?;

        if let Value::Integer(code) = args[0] {
            exit_code = code;
        } else {
            exit_code = 1;
        }
    }

    println!("Goodbye, Friend");

    std::process::exit(i32::try_from(exit_code).expect("Exit code shouldn't be out of bounds"));
}

pub fn builtin_print(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("PRINT", 1, args, ArgComp::Eq)?;

    let evaluated = eval_expression(&args[0], env)?;

    println!("{evaluated}");

    Ok(evaluated)
}

pub fn builtin_format(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("FORMAT", 2, args, ArgComp::Min)?;

    let should_print = eval_expression(&args[0], env)?;

    let should_print = match should_print {
        Value::True => true,
        Value::Nil => false,
        _ => {
            wrong_type!(should_print.to_string(), Type::Boolean)
        }
    };

    let format_string = eval_expression(&args[1], env)?;

    let Value::String(format_string) = format_string else {
        return Err(EvalError::WrongType(
            format_string.to_string(),
            Type::String,
        ));
    };

    let chars: Vec<_> = format_string.chars().collect();
    let mut buffer = String::new();
    let mut params = args[2..].iter().cloned().rev().collect::<Vec<_>>();
    let mut idx = 0;

    while idx < chars.len() {
        let ch = chars[idx];

        if ch == '~'
            && idx + 1 < format_string.len()
            && ['a', 'b', 'd', 'o', 'x', '$', '%'].contains(&chars[idx + 1])
        {
            if chars[idx + 1] == '%' {
                buffer.push('\n');

                idx += 2;

                continue;
            }

            let value = params.pop().ok_or(EvalError::Other(
                "Out of chars while evaluating format string".to_string(),
            ))?;
            let param = eval_expression(&value, env)?;
            let next = chars[idx + 1];

            match next {
                'a' => buffer.push_str(&param.to_string()),
                'b' | 'o' | 'd' | 'x' => match param {
                    Value::Integer(i) => match next {
                        'b' => buffer.push_str(&format!("{i:b}")),
                        'o' => buffer.push_str(&format!("{i:o}")),
                        'd' => buffer.push_str(&format!("{i}")),
                        'x' => buffer.push_str(&format!("{i:x}")),
                        _ => unreachable!(),
                    },
                    _ => wrong_type!(param.to_string(), Type::Integer),
                },
                _ => {}
            }

            idx += 1;
        } else {
            buffer.push(ch);
        }

        idx += 1;
    }

    if should_print {
        print!("{buffer}");

        Ok(Value::Nil)
    } else {
        Ok(Value::String(buffer))
    }
}

pub fn builtin_quote(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("QUOTE", 1, args, ArgComp::Eq)?;

    eval_expression(&Value::Quoted(Rc::new(args[0].clone())), env)
}

pub fn builtin_char_code(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("CHAR-CODE", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    match arg {
        Value::Char(c) => Ok(Value::Integer(Int::from(c.code()))),
        _ => wrong_type!(arg.to_string(), Type::Char),
    }
}

pub fn builtin_code_char(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("CODE-CHAR", 1, args, ArgComp::Eq)?;

    let arg = eval_expression(&args[0], env)?;

    match arg {
        Value::Integer(i) => Ok(Value::Char(Char::from(u8::try_from(i)?))),
        _ => wrong_type!(arg.to_string(), Type::Integer),
    }
}

pub fn builtin_defstruct(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("DEFSTRUCT", 1, args, ArgComp::Min)?;

    let Value::Symbol(name) = &args[0] else {
        return Err(EvalError::WrongType(args[0].to_string(), Type::Symbol));
    };

    let fields = args[1..]
        .iter()
        .map(|field| match field {
            Value::Symbol(s) => Ok(s.to_string()),
            _ => wrong_type!(field.to_string(), Type::Symbol),
        })
        .collect::<Result<Vec<_>, _>>()?;

    let struct_def = Value::StructDef(name.clone(), fields.clone());

    env.set(&name.to_lowercase(), struct_def.clone());
    env.set(
        &format!("make-{}", name.to_lowercase()),
        native_fn!(builtin_make_struct),
    );
    env.set(
        &format!("copy-{}", name.to_lowercase()),
        native_fn!(builtin_copy_struct),
    );

    for field in fields {
        env.set(
            &format!("{}-{field}", name.to_lowercase()),
            native_fn!(builtin_get_struct_field),
        );
    }

    Ok(struct_def)
}

pub fn builtin_make_struct(args: &[Value], env: &mut Env) -> ValRes {
    let Value::String(func_name) = env
        .get("$curr_func_name$")
        .ok_or(EvalError::Other("Impossible".to_string()))?
        .inner()
    else {
        panic!();
    };
    let name = func_name.trim_start_matches("make-").to_string();
    let name = env.get(&name).unwrap().inner();

    let Value::StructDef(name, fields) = eval_expression(&name, env)? else {
        return Err(EvalError::Other(format!("{name} is not a STRUCT")));
    };

    if args.len() % 2 != 0 {
        return Err(EvalError::Other(format!(
            "invalid number of arguments given to MAKE-{}",
            name.to_uppercase()
        )));
    }

    let mut fields_values = vec![];

    for chunk in args.chunks(2) {
        let Value::Symbol(key) = &chunk[0] else {
            return Err(EvalError::WrongType(chunk[0].to_string(), Type::Symbol));
        };
        let key = key.trim_start_matches(':').to_string();

        if !fields.contains(&key) {
            return Err(EvalError::Other(format!(
                "illegal keyword/value pair {key} for MAKE-{}",
                name.to_uppercase()
            )));
        }

        let value = eval_expression(&chunk[1], env)?;

        fields_values.push((key, value));
    }

    let mut values = fields_values.iter().cloned().collect::<HashMap<_, _>>();

    for field in &fields {
        if !values.contains_key(field) {
            values.insert(field.to_string(), Value::Nil);
        }
    }

    let new_struct = Struct { name, values };

    Ok(Value::Struct(new_struct))
}

pub fn builtin_copy_struct(args: &[Value], env: &mut Env) -> ValRes {
    let Value::String(func_name) = env
        .get("$curr_func_name$")
        .ok_or(EvalError::Other("Impossible".to_string()))?
        .inner()
    else {
        panic!();
    };

    assert_args(&func_name.to_uppercase(), 1, args, ArgComp::Min)?;

    let arg = eval_expression(&args[0], env)?;
    let name = func_name.trim_start_matches("copy-").to_string();

    match arg {
        Value::Struct(_) => Ok(arg),
        _ => wrong_type!(arg.to_string(), Type::Struct(name)),
    }
}

pub fn builtin_get_struct_field(args: &[Value], env: &mut Env) -> ValRes {
    let Value::String(func_name) = env
        .get("$curr_func_name$")
        .ok_or(EvalError::Other("Impossible".to_string()))?
        .inner()
    else {
        panic!();
    };

    assert_args(&func_name.to_uppercase(), 1, args, ArgComp::Min)?;

    let Value::Struct(s) = eval_expression(&args[0], env)? else {
        return Err(EvalError::Other(format!("{} is not a STRUCT", args[0])));
    };

    let key = func_name
        .strip_prefix(&format!("{}-", s.name))
        .unwrap_or(&func_name);

    s.values.get(key).cloned().ok_or(EvalError::Other(format!(
        "Invalid key :{key} for struct {}",
        s.name
    )))
}

pub fn builtin_set_struct_field(args: &[Value], env: &mut Env) -> ValRes {
    let Value::Symbol(symbol) = &args[0] else {
        return Err(EvalError::WrongType(args[0].to_string(), Type::Symbol));
    };

    let Value::Struct(mut s) = eval_expression(&args[1], env)? else {
        return Err(EvalError::Other(format!("{} is not a STRUCT", args[1])));
    };

    let key = symbol
        .strip_prefix(&format!("{}-", s.name))
        .unwrap_or(symbol)
        .to_string();
    let value = eval_expression(&args[2], env)?;

    s.values.insert(key, value);

    let res = Value::Struct(s);

    builtin_setf(&[args[1].clone(), res.clone()], env)?;

    Ok(res)
}

pub fn builtin_progn(args: &[Value], env: &mut Env) -> ValRes {
    let mut result = Value::Nil;
    let mut new_env = Env::extend(env);

    for value in args {
        result = eval_expression(value, &mut new_env)?;
    }

    Ok(result)
}

pub fn builtin_let(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("LET", 2, args, ArgComp::Min)?;

    let decls = &args[0];
    let mut body = args[1..].to_vec();
    body.insert(0, Value::Symbol("progn".to_string()));
    let body = Value::List(List(body));

    let Value::List(List(decls)) = decls else {
        wrong_type!(decls.to_string(), Type::List);
    };

    let mut params = Vec::new();
    let mut args = Vec::new();

    for decl in decls {
        let Value::List(List(decl)) = decl else {
            return Err(EvalError::WrongType(decl.to_string(), Type::List));
        };
        assert_args("LET", 2, decl, ArgComp::Eq)?;

        let sym = &decl[0];
        let val = &decl[1];

        let Value::Symbol(sym) = sym else {
            return Err(EvalError::WrongType(sym.to_string(), Type::Symbol));
        };

        let mut new_env = Env::extend(env);
        let val = eval_expression(val, &mut new_env)?;

        params.push(sym.to_string());
        args.push(val);
    }

    let lambda = Function::Lambda(Lambda {
        params,
        body: Rc::new(body),
    });

    eval_function("", lambda, &args, env)
}

pub fn builtin_setf(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("SETF", 2, args, ArgComp::Eq)?;

    match &args[0] {
        Value::Symbol(s) => {
            let val = eval_expression(&args[1], env)?;

            if let Some(error) = env.set(s, val.clone()) {
                return Err(EvalError::Other(error));
            }

            Ok(val)
        }
        Value::List(List(l))
            if l.len() == 3 && l[0] == Value::Symbol("gethash".to_string()) && l[2].is_symbol() =>
        {
            builtin_set_hash(&[l[1].clone(), l[2].clone(), args[1].clone()], env)?;

            eval_expression(&args[1], env)
        }
        Value::List(List(l)) if l.len() == 2 && l[0].is_symbol() => {
            builtin_set_struct_field(&[l[0].clone(), l[1].clone(), args[1].clone()], env)?;

            eval_expression(&args[1], env)
        }
        _ => wrong_type!(args[0].to_string(), Type::Symbol),
    }
}

pub fn builtin_defconstant(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("DEFCONSTANT", 2, args, ArgComp::Eq)?;

    match &args[0] {
        Value::Symbol(s) => {
            let val = eval_expression(&args[1], env)?;

            env.constant(s, val.clone());

            Ok(val)
        }
        _ => wrong_type!(args[0].to_string(), Type::Symbol),
    }
}

pub fn builtin_lambda(args: &[Value]) -> ValRes {
    assert_args("LAMBDA", 2, args, ArgComp::Eq)?;

    let params = &args[0];
    let body = &args[1];

    let Value::List(List(params)) = params else {
        wrong_type!(params.to_string(), Type::List)
    };

    let mut param_names = Vec::new();

    for param in params {
        match param {
            Value::Symbol(s) => param_names.push(s.clone()),
            _ => wrong_type!(param.to_string(), Type::Symbol),
        }
    }

    let lambda = Value::Function(Function::Lambda(Lambda {
        params: param_names,
        body: Rc::new(body.clone()),
    }));

    Ok(lambda)
}

pub fn builtin_defun(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("DEFUN", 3, args, ArgComp::Eq)?;

    let list = vec![args[1].clone(), args[2].clone()];
    let lambda = builtin_lambda(&list)?;

    builtin_setf(&[args[0].clone(), lambda], env)
}

pub fn builtin_load(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("LOAD", 1, args, ArgComp::Eq)?;

    let Value::String(path) = &args[0] else {
        return Err(EvalError::FilePathMustBeString(args[0].clone()));
    };

    let Ok(program) = fs::read_to_string(path) else {
        return Err(EvalError::FileNotFound(path.to_string()));
    };

    let res = eval(&program, env)?;

    eval_expression(&res, env)
}

pub fn builtin_read(_args: &[Value], _env: &mut Env) -> ValRes {
    let mut program = String::new();

    std::io::stdin().read_line(&mut program)?;

    eval_sexpression(&parse(&program)?)
}

pub fn builtin_eval(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("EVAL", 1, args, ArgComp::Eq)?;

    let value = eval_expression(&args[0], env)?;

    match &value {
        Value::List(List(l)) => {
            let list = l
                .iter()
                .map(|v| match v {
                    Value::Debug(s) => Value::Symbol(s.clone()),
                    _ => v.clone(),
                })
                .collect();

            eval_expression(&Value::List(List(list)), env)
        }
        _ => Ok(value),
    }
}
