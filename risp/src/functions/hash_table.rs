use crate::{
    env::Env,
    eval::{assert_args, eval_expression, eval_function, ArgComp},
    types::{Type, ValRes, Value},
    wrong_type,
};

use super::builtin_setf;

pub fn builtin_make_hash_table(args: &[Value], _env: &mut Env) -> ValRes {
    assert_args("MAKE-HASH-TABLE", 0, args, ArgComp::Eq)?;

    Ok(Value::HashTable(vec![]))
}

pub fn builtin_get_hash(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("GETHASH", 2, args, ArgComp::Eq)?;

    let key = eval_expression(&args[0], env)?;

    let Value::HashTable(hash_table) = eval_expression(&args[1], env)? else {
        wrong_type!(args[1].to_string(), Type::HashTable);
    };

    Ok(hash_table
        .iter()
        .find(|(k, _)| *k == key)
        .map_or(Value::Nil, |(_, v)| v.clone()))
}

pub fn builtin_set_hash(args: &[Value], env: &mut Env) -> ValRes {
    let key = eval_expression(&args[0], env)?;
    let value = eval_expression(&args[2], env)?;

    let Value::HashTable(hash_table) = eval_expression(&args[1], env)? else {
        wrong_type!(args[1].to_string(), Type::HashTable);
    };

    let mut new_hash_table: Vec<_> = hash_table
        .iter()
        .filter(|(k, _)| *k != key)
        .cloned()
        .collect();

    new_hash_table.push((key, value));

    builtin_setf(&[args[1].clone(), Value::HashTable(new_hash_table)], env)
}

pub fn builtin_rem_hash(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("REMHASH", 2, args, ArgComp::Eq)?;

    let key = eval_expression(&args[0], env)?;

    let Value::HashTable(hash_table) = eval_expression(&args[1], env)? else {
        wrong_type!(args[1].to_string(), Type::HashTable);
    };

    let new_hash_table = hash_table
        .iter()
        .filter(|(k, _)| *k != key)
        .cloned()
        .collect();

    builtin_setf(&[args[1].clone(), Value::HashTable(new_hash_table)], env)
}

pub fn builtin_map_hash(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("MAPHASH", 2, args, ArgComp::Eq)?;

    let Value::Function(function) = eval_expression(&args[0], env)? else {
        wrong_type!(args[0].to_string(), Type::HashTable);
    };

    let Value::HashTable(hash_table) = eval_expression(&args[1], env)? else {
        wrong_type!(args[1].to_string(), Type::HashTable);
    };

    for (k, v) in hash_table {
        eval_function("", function.clone(), &[k.clone(), v.clone()], env)?;
    }

    Ok(Value::Nil)
}
