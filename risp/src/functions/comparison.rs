use crate::{
    env::Env,
    eval::{assert_args, eval_expression, ArgComp},
    types::{
        Float as Real, Type, ValRes,
        Value::{self, *},
    },
    wrong_type,
};

pub fn builtin_eq(args: &[Value], env: &mut Env) -> ValRes {
    let mut curr = Nil;

    for value in args {
        let value = eval_expression(value, env)?;

        let cond = match (&curr, &value) {
            (Nil, _) if value.is_number() => {
                curr = value.clone();
                true
            }
            (Integer(l), Integer(r)) => *l == *r,
            (Float(l), Float(r)) => *l == *r,
            (Integer(l), Float(r)) => *l as Real == *r,
            (Float(l), Integer(r)) => *l == *r as Real,
            _ => wrong_type!(value.to_string(), Type::Number),
        };

        if !cond {
            return Ok(Nil);
        }
    }

    Ok(True)
}

pub fn builtin_ne(args: &[Value], env: &mut Env) -> ValRes {
    let mut curr = Nil;

    for value in args {
        let value = eval_expression(value, env)?;

        let cond = match (&curr, &value) {
            (Nil, _) if value.is_number() => {
                curr = value.clone();
                true
            }
            (Integer(l), Integer(r)) => *l != *r,
            (Float(l), Float(r)) => *l != *r,
            (Integer(l), Float(r)) => *l as Real != *r,
            (Float(l), Integer(r)) => *l != *r as Real,
            _ => wrong_type!(value.to_string(), Type::Number),
        };

        if !cond {
            return Ok(Nil);
        }
    }

    Ok(True)
}

pub fn builtin_gt(args: &[Value], env: &mut Env) -> ValRes {
    let mut curr = Nil;

    for value in args {
        let value = eval_expression(value, env)?;

        let cond = match (&curr, &value) {
            (Nil, _) if value.is_number() => {
                curr = value.clone();
                true
            }
            (Integer(l), Integer(r)) => *l > *r,
            (Float(l), Float(r)) => *l > *r,
            (Integer(l), Float(r)) => *l as Real > *r,
            (Float(l), Integer(r)) => *l > *r as Real,
            _ => wrong_type!(value.to_string(), Type::Number),
        };

        if !cond {
            return Ok(Nil);
        }
    }

    Ok(True)
}

pub fn builtin_lt(args: &[Value], env: &mut Env) -> ValRes {
    let mut curr = Nil;

    for value in args {
        let value = eval_expression(value, env)?;

        let cond = match (&curr, &value) {
            (Nil, _) if value.is_number() => {
                curr = value.clone();
                true
            }
            (Integer(l), Integer(r)) => *l < *r,
            (Float(l), Float(r)) => *l < *r,
            (Integer(l), Float(r)) => (*l as Real) < *r,
            (Float(l), Integer(r)) => *l < *r as Real,
            _ => wrong_type!(value.to_string(), Type::Number),
        };

        if !cond {
            return Ok(Nil);
        }
    }

    Ok(True)
}

pub fn builtin_ge(args: &[Value], env: &mut Env) -> ValRes {
    let mut curr = Nil;

    for value in args {
        let value = eval_expression(value, env)?;

        let cond = match (&curr, &value) {
            (Nil, _) if value.is_number() => {
                curr = value.clone();
                true
            }
            (Integer(l), Integer(r)) => *l >= *r,
            (Float(l), Float(r)) => *l >= *r,
            (Integer(l), Float(r)) => *l as Real >= *r,
            (Float(l), Integer(r)) => *l >= *r as Real,
            _ => wrong_type!(value.to_string(), Type::Number),
        };

        if !cond {
            return Ok(Nil);
        }
    }

    Ok(True)
}

pub fn builtin_le(args: &[Value], env: &mut Env) -> ValRes {
    let mut curr = Nil;

    for value in args {
        let value = eval_expression(value, env)?;

        let cond = match (&curr, &value) {
            (Nil, _) if value.is_number() => {
                curr = value.clone();
                true
            }
            (Integer(l), Integer(r)) => *l <= *r,
            (Float(l), Float(r)) => *l <= *r,
            (Integer(l), Float(r)) => (*l as Real) <= *r,
            (Float(l), Integer(r)) => *l <= *r as Real,
            _ => wrong_type!(value.to_string(), Type::Number),
        };

        if !cond {
            return Ok(Nil);
        }
    }

    Ok(True)
}

pub fn builtin_max(args: &[Value], env: &mut Env) -> ValRes {
    let mut curr = Nil;

    for value in args {
        let value = eval_expression(value, env)?;

        curr = match (&curr, &value) {
            (Nil, _) if value.is_number() => value.clone(),
            (Integer(l), Integer(r)) => Integer(*l.max(r)),
            (Float(l), Float(r)) => Float(l.max(*r)),
            (Integer(l), Float(r)) => {
                if *l as Real >= *r {
                    curr.clone()
                } else {
                    value.clone()
                }
            }
            (Float(l), Integer(r)) => {
                if *l >= *r as Real {
                    curr.clone()
                } else {
                    value.clone()
                }
            }
            _ => wrong_type!(value.to_string(), Type::Number),
        }
    }

    Ok(curr)
}

pub fn builtin_min(args: &[Value], env: &mut Env) -> ValRes {
    let mut curr = Nil;

    for value in args {
        let value = eval_expression(value, env)?;

        curr = match (&curr, &value) {
            (Nil, _) if value.is_number() => value.clone(),
            (Integer(l), Integer(r)) => Integer(*l.min(r)),
            (Float(l), Float(r)) => Float(l.min(*r)),
            (Integer(l), Float(r)) => {
                if *l as Real <= *r {
                    curr.clone()
                } else {
                    value.clone()
                }
            }
            (Float(l), Integer(r)) => {
                if *l <= *r as Real {
                    curr.clone()
                } else {
                    value.clone()
                }
            }
            _ => wrong_type!(value.to_string(), Type::Number),
        }
    }

    Ok(curr)
}

pub fn builtin_char_eq(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("CHAR=", 1, args, ArgComp::Min)?;

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, Char(_)) if value.is_char() => curr = value.clone(),
            (Char(l), Char(r)) => {
                if l.code() != r.code() {
                    return Ok(Nil);
                }
            }
            _ => wrong_type!(value.to_string(), Type::Char),
        }
    }

    Ok(True)
}

pub fn builtin_char_ne(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("CHAR/=", 1, args, ArgComp::Min)?;

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, Char(_)) if value.is_char() => curr = value.clone(),
            (Char(l), Char(r)) => {
                if l.code() == r.code() {
                    return Ok(Nil);
                }
            }
            _ => wrong_type!(value.to_string(), Type::Char),
        }
    }

    Ok(True)
}

pub fn builtin_char_gt(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("CHAR>", 1, args, ArgComp::Min)?;

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, Char(_)) if value.is_char() => curr = value.clone(),
            (Char(l), Char(r)) => {
                if l.code() <= r.code() {
                    return Ok(Nil);
                }
            }
            _ => wrong_type!(value.to_string(), Type::Char),
        }
    }

    Ok(True)
}

pub fn builtin_char_lt(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("CHAR<", 1, args, ArgComp::Min)?;

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, Char(_)) if value.is_char() => curr = value.clone(),
            (Char(l), Char(r)) => {
                if l.code() >= r.code() {
                    return Ok(Nil);
                }
            }
            _ => wrong_type!(value.to_string(), Type::Char),
        }
    }

    Ok(True)
}

pub fn builtin_char_ge(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("CHAR>=", 1, args, ArgComp::Min)?;

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, Char(_)) if value.is_char() => curr = value.clone(),
            (Char(l), Char(r)) => {
                if l.code() < r.code() {
                    return Ok(Nil);
                }
            }
            _ => wrong_type!(value.to_string(), Type::Char),
        }
    }

    Ok(True)
}

pub fn builtin_char_le(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("CHAR<=", 1, args, ArgComp::Min)?;

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, Char(_)) if value.is_char() => curr = value.clone(),
            (Char(l), Char(r)) => {
                if l.code() > r.code() {
                    return Ok(Nil);
                }
            }
            _ => wrong_type!(value.to_string(), Type::Char),
        }
    }

    Ok(True)
}

pub fn builtin_string_eq(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("STRING=", 1, args, ArgComp::Min)?;

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, String(_)) if value.is_string() => curr = value.clone(),
            (String(l), String(r)) => {
                if l != r {
                    return Ok(Nil);
                }
            }
            _ => wrong_type!(value.to_string(), Type::String),
        }
    }

    Ok(True)
}

pub fn builtin_string_ne(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("STRING/=", 1, args, ArgComp::Min)?;

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, String(_)) if value.is_string() => curr = value.clone(),
            (String(l), String(r)) => {
                if l == r {
                    return Ok(Nil);
                }
            }
            _ => wrong_type!(value.to_string(), Type::String),
        }
    }

    Ok(True)
}

pub fn builtin_string_gt(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("STRING>", 1, args, ArgComp::Min)?;

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, String(_)) if value.is_string() => curr = value.clone(),
            (String(l), String(r)) => {
                if l <= r {
                    return Ok(Nil);
                }
            }
            _ => wrong_type!(value.to_string(), Type::String),
        }
    }

    Ok(True)
}

pub fn builtin_string_lt(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("STRING<", 1, args, ArgComp::Min)?;

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, String(_)) if value.is_string() => curr = value.clone(),
            (String(l), String(r)) => {
                if l >= r {
                    return Ok(Nil);
                }
            }
            _ => wrong_type!(value.to_string(), Type::String),
        }
    }

    Ok(True)
}

pub fn builtin_string_ge(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("STRING>=", 1, args, ArgComp::Min)?;

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, String(_)) if value.is_string() => curr = value.clone(),
            (String(l), String(r)) => {
                if l < r {
                    return Ok(Nil);
                }
            }
            _ => wrong_type!(value.to_string(), Type::String),
        }
    }

    Ok(True)
}

pub fn builtin_string_le(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("STRING<=", 1, args, ArgComp::Min)?;

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, String(_)) if value.is_string() => curr = value.clone(),
            (String(l), String(r)) => {
                if l > r {
                    return Ok(Nil);
                }
            }
            _ => wrong_type!(value.to_string(), Type::String),
        }
    }

    Ok(True)
}
