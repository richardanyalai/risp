use crate::{
    env::Env,
    eval::{assert_args, eval_expression, ArgComp},
    functions::builtin_setf,
    types::{
        Float as Real, Type, ValRes,
        Value::{self, *},
    },
    wrong_type,
};

macro_rules! float {
    ($a:expr, $op:tt, $b:expr) => {
        Value::Float((($a * 10000000.0) $op ($b * 10000000.0)) / 10000000.0)
    };
}

pub fn builtin_add(args: &[Value], env: &mut Env) -> ValRes {
    if args.is_empty() {
        return Ok(Integer(0));
    }

    let mut curr = Nil;

    for value in args {
        let value = eval_expression(value, env)?;

        curr = match (&curr, &value) {
            (Nil, _) if value.is_number() => value.clone(),
            (Integer(l), Integer(r)) => Integer(l + r),
            (Float(l), Float(r)) => float!(l, +, r),
            (Integer(l), Float(r)) => float!(*l as Real, +, r),
            (Float(l), Integer(r)) => float!(l, +, *r as Real),
            _ => wrong_type!(value.to_string(), Type::Number),
        }
    }

    Ok(curr)
}

pub fn builtin_sub(args: &[Value], env: &mut Env) -> ValRes {
    if args.len() == 1 {
        let value = eval_expression(&args[0], env)?;

        return match value {
            Integer(i) => Ok(Integer(-i)),
            Float(f) => Ok(Float(-f)),
            _ => wrong_type!(value.to_string(), Type::Number),
        };
    }

    let mut curr = Nil;

    for value in args {
        let value = eval_expression(value, env)?;

        curr = match (&curr, &value) {
            (Nil, _) if value.is_number() => value.clone(),
            (Integer(l), Integer(r)) => Integer(l - r),
            (Float(l), Float(r)) => float!(l, -, r),
            (Integer(l), Float(r)) => float!(*l as Real, -, r),
            (Float(l), Integer(r)) => float!(l, -, *r as Real),
            _ => wrong_type!(value.to_string(), Type::Number),
        }
    }

    Ok(curr)
}

pub fn builtin_mul(args: &[Value], env: &mut Env) -> ValRes {
    let mut curr = Integer(1);

    for value in args {
        let value = eval_expression(value, env)?;

        curr = match (&curr, &value) {
            (Nil, _) if value.is_number() => value.clone(),
            (Integer(l), Integer(r)) => Integer(l * r),
            (Float(l), Float(r)) => Float(l * r),
            (Integer(l), Float(r)) => Float(*l as Real * r),
            (Float(l), Integer(r)) => Float(l * *r as Real),
            _ => wrong_type!(value.to_string(), Type::Number),
        };
    }

    Ok(curr)
}

pub fn builtin_div(args: &[Value], env: &mut Env) -> ValRes {
    if args.len() == 1 {
        let value = eval_expression(&args[0], env)?;

        return match &value {
            Integer(i) => Ok(Integer(*i)),
            Float(f) => Ok(Float(*f)),
            _ => wrong_type!(value.to_string(), Type::Number),
        };
    }

    let mut curr = Nil;
    let mut res_float = 0.0;

    for value in args {
        let value = eval_expression(value, env)?;

        curr = match (&curr, &value) {
            (Nil, _) if value.is_number() => {
                res_float = match value {
                    Integer(i) => i as Real,
                    Float(f) => f,
                    _ => 0.0,
                };
                value.clone()
            }
            (Integer(l), Integer(r)) => {
                res_float /= *r as Real;
                Integer(l / r)
            }
            (Float(l), Float(r)) => {
                res_float /= r;
                Float(l / r)
            }
            (Integer(_), Float(r)) => {
                res_float /= r;
                Float(res_float)
            }
            (Float(_), Integer(r)) => {
                res_float /= *r as Real;
                Float(res_float)
            }
            _ => wrong_type!(value.to_string(), Type::Number),
        };
    }

    Ok(curr)
}

pub fn builtin_mod(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("MOD", 2, args, ArgComp::Eq)?;

    let left = eval_expression(&args[0], env)?;
    let right = eval_expression(&args[1], env)?;

    let res = match (&left, &right) {
        (Integer(l), Integer(r)) => Integer(l % r),
        (Float(l), Float(r)) => Float(l % r),
        (Integer(l), Float(r)) => Float(*l as Real % r),
        (Float(l), Integer(r)) => Float(l % *r as Real),
        _ => {
            wrong_type!(
                if left.is_number() {
                    right.to_string()
                } else {
                    left.to_string()
                },
                Type::Number
            )
        }
    };

    Ok(res)
}

pub fn builtin_rem(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("REM", 2, args, ArgComp::Eq)?;

    let left = eval_expression(&args[0], env)?;
    let right = eval_expression(&args[1], env)?;

    let res = match (&left, &right) {
        (Integer(l), Integer(r)) => Integer(l - (l / r) * r),
        (Float(l), Float(r)) => Float(l - (l / r) * r),
        (Integer(l), Float(r)) => Float({
            let l = *l as Real;
            l - (l / r) * r
        }),
        (Float(l), Integer(r)) => Float({
            let r = *r as Real;
            l - (l / r) * r
        }),
        _ => {
            wrong_type!(
                if left.is_number() {
                    right.to_string()
                } else {
                    left.to_string()
                },
                Type::Number
            )
        }
    };

    Ok(res)
}

pub fn builtin_add_one(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("1+", 1, args, ArgComp::Eq)?;

    let operand = eval_expression(&args[0], env)?;

    let res = match operand {
        Integer(i) => Integer(i + 1),
        Float(f) => float!(f, +, 1.0),
        _ => wrong_type!(operand.to_string(), Type::Number),
    };

    Ok(res)
}

pub fn builtin_sub_one(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("1-", 1, args, ArgComp::Eq)?;

    let operand = eval_expression(&args[0], env)?;

    let res = match operand {
        Integer(i) => Integer(i - 1),
        Float(f) => float!(f, -, 1.0),
        _ => wrong_type!(operand.to_string(), Type::Number),
    };

    Ok(res)
}

pub fn builtin_incf(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("INCF", 1, args, ArgComp::Min)?;
    assert_args("INCF", 2, args, ArgComp::Max)?;

    let res = if args.len() == 2 {
        builtin_add(args, env)?
    } else {
        builtin_add_one(args, env)?
    };

    builtin_setf(&[args[0].clone(), res], env)
}

pub fn builtin_decf(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("DECF", 1, args, ArgComp::Min)?;
    assert_args("DECF", 2, args, ArgComp::Max)?;

    let res = if args.len() == 2 {
        builtin_sub(args, env)?
    } else {
        builtin_sub_one(args, env)?
    };

    builtin_setf(&[args[0].clone(), res], env)
}
