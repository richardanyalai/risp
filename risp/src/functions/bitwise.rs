use crate::{
    env::Env,
    eval::{assert_args, eval_expression, ArgComp},
    types::{
        Type, ValRes,
        Value::{self, *},
    },
    wrong_type,
};

pub fn builtin_logand(args: &[Value], env: &mut Env) -> ValRes {
    if args.is_empty() {
        return Ok(Integer(-1));
    }

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, Integer(_)) => curr = value.clone(),
            (Integer(l), Integer(r)) => curr = Integer(l & r),
            _ => wrong_type!(value.to_string(), Type::Integer),
        }
    }

    Ok(curr)
}

pub fn builtin_logior(args: &[Value], env: &mut Env) -> ValRes {
    if args.is_empty() {
        return Ok(Integer(0));
    }

    let mut curr = 0;

    for value in args {
        match &eval_expression(value, env)? {
            Integer(i) => curr |= i,
            _ => wrong_type!(value.to_string(), Type::Integer),
        }
    }

    Ok(Integer(curr))
}

pub fn builtin_lognot(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("LOGNOT", 1, args, ArgComp::Eq)?;

    match eval_expression(&args[0], env)? {
        Integer(i) => Ok(Integer(!i)),
        _ => wrong_type!(args[0].to_string(), Type::Integer),
    }
}

pub fn builtin_logxor(args: &[Value], env: &mut Env) -> ValRes {
    if args.is_empty() {
        return Ok(Integer(0));
    }

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, Integer(_)) => curr = value.clone(),
            (Integer(l), Integer(r)) => curr = Integer(l ^ r),
            _ => wrong_type!(value.to_string(), Type::Integer),
        }
    }

    Ok(curr)
}

pub fn builtin_lognand(args: &[Value], env: &mut Env) -> ValRes {
    if args.is_empty() {
        return Ok(Integer(-1));
    }

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, Integer(_)) => curr = value.clone(),
            (Integer(l), Integer(r)) => curr = Integer(!(l & r)),
            _ => wrong_type!(value.to_string(), Type::Integer),
        }
    }

    Ok(curr)
}

pub fn builtin_lognor(args: &[Value], env: &mut Env) -> ValRes {
    if args.is_empty() {
        return Ok(Integer(-1));
    }

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, Integer(_)) => curr = value.clone(),
            (Integer(l), Integer(r)) => curr = Integer(!(l | r)),
            _ => wrong_type!(value.to_string(), Type::Integer),
        }
    }

    Ok(curr)
}

pub fn builtin_logeqv(args: &[Value], env: &mut Env) -> ValRes {
    if args.is_empty() {
        return Ok(Integer(-1));
    }

    let mut curr = Nil;

    for value in args {
        match (&curr, &eval_expression(value, env)?) {
            (Nil, Integer(_)) => curr = value.clone(),
            (Integer(l), Integer(r)) => curr = Integer(!(l ^ r)),
            _ => wrong_type!(value.to_string(), Type::Integer),
        }
    }

    Ok(curr)
}

pub fn builtin_ash(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("ASH", 2, args, ArgComp::Eq)?;

    let value = eval_expression(&args[0], env)?;
    let shift = eval_expression(&args[1], env)?;

    if let (Integer(v), Integer(s)) = (&value, &shift) {
        Ok(Integer(v >> s))
    } else {
        if value.is_number() {
            wrong_type!(shift.to_string(), Type::Integer);
        }

        wrong_type!(value.to_string(), Type::Integer);
    }
}
