use crate::{
    env::Env,
    eval::{assert_args, eval_expression, ArgComp},
    functions::builtin_setf,
    types::{EvalError, Float, Int, List, Type, ValRes, Value},
    wrong_type,
};

pub fn builtin_loop(args: &[Value], env: &mut Env) -> ValRes {
    if !args.is_empty() && args[0] == Value::Symbol("while".to_string()) {
        return builtin_loop_while(args, env);
    }

    if !args.is_empty() && args[0] == Value::Symbol("for".to_string()) {
        return builtin_loop_for(args, env);
    }

    let env = &mut Env::extend(env);

    loop {
        for value in args {
            if let Value::Return(ret) = eval_expression(value, env)? {
                return Ok(ret.as_ref().clone());
            }
        }
    }
}

fn builtin_loop_while(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("LOOP WHILE", 2, args, ArgComp::Min)?;

    let env = &mut Env::extend(env);

    if let [_, test, ..] = &args {
        let offset = if args.len() > 2 {
            if args[2] != Value::Symbol("do".to_string()) {
                return Err(EvalError::Other(
                    "Missing DO keyword in LOOP WHILE".to_string(),
                ));
            }

            3
        } else {
            2
        };

        loop {
            let test = eval_expression(test, env)?;

            if test.is_null() {
                return Ok(Value::Nil);
            }

            for value in &args[offset..] {
                if let Value::Return(ret) = eval_expression(value, env)? {
                    return Ok(ret.as_ref().clone());
                }
            }
        }
    }

    Ok(Value::Nil)
}

fn builtin_loop_for(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("LOOP FOR", 4, args, ArgComp::Min)?;

    let Value::Symbol(sym) = &args[2] else {
        wrong_type!(args[2].to_string(), Type::Symbol);
    };

    let env = &mut Env::extend(env);

    if sym.to_lowercase() == "from" {
        if let [_for, var, _from, range_start, _, range_end, _do, ..] = &args {
            let mul = 10_000_000;

            let range: Vec<Value> = match (range_start, range_end) {
                (Value::Integer(start), Value::Integer(end)) => {
                    (*start..=*end).map(Value::Integer).collect()
                }
                (Value::Float(start), Value::Float(end)) => {
                    let start = (*start * mul as Float) as Int;
                    let end = (*end * mul as Float) as Int;

                    (start..=end)
                        .map(|i| Value::Float((i as Float) / mul as Float))
                        .collect()
                }
                (Value::Integer(start), Value::Float(end)) => {
                    let start = *start * mul;
                    let end = (*end * mul as Float) as Int;

                    (start..=end)
                        .map(|i| Value::Float((i as Float) / mul as Float))
                        .collect()
                }
                (Value::Float(start), Value::Integer(end)) => {
                    let start = (*start * mul as Float) as Int;
                    let end = *end * mul;

                    (start..=end)
                        .map(|i| Value::Float((i as Float) / mul as Float))
                        .collect()
                }
                _ => {
                    wrong_type!(
                        if range_start.is_number() {
                            range_end
                        } else {
                            range_start
                        }
                        .to_string(),
                        Type::Number
                    )
                }
            };

            let body = &args[7..];

            for i in range {
                builtin_setf(&[var.clone(), i.clone()], env)?;

                for line in body {
                    if let Value::Return(v) = eval_expression(line, env)? {
                        return Ok(v.as_ref().clone());
                    }
                }
            }
        }
    } else if sym.to_lowercase() == "in" {
        if let [_for, var, _in, list, _do, ..] = &args {
            let list = eval_expression(list, env)?;
            let Value::List(List(list)) = list else {
                wrong_type!(list.to_string(), Type::List)
            };

            let body = &args[5..];

            for i in list {
                builtin_setf(&[var.clone(), i.clone()], env)?;

                for line in body {
                    if let Value::Return(v) = eval_expression(line, env)? {
                        return Ok(v.as_ref().clone());
                    }
                }
            }
        }
    } else {
        return Err(EvalError::Other(format!(
            "unknown keyword {}",
            sym.to_uppercase()
        )));
    }

    Ok(Value::Nil)
}

pub fn builtin_dotimes(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("DOTIMES", 1, args, ArgComp::Min)?;

    let invalid_header = "Invalid DOTIMES header";
    let env = &mut Env::extend(env);

    let Value::List(List(header)) = &args[0] else {
        return Err(EvalError::Other(format!("{invalid_header}: {}", args[0])));
    };

    let [symbol, counter] = header.as_slice() else {
        return Err(EvalError::Other(format!(
            "{invalid_header}: {}",
            List(header.clone())
        )));
    };

    let Value::Integer(c) = eval_expression(counter, env)? else {
        return Err(EvalError::Other(format!(
            "Invalid DOTIMES counter: {counter}"
        )));
    };

    for i in 0..c {
        for value in &args[1..] {
            builtin_setf(&[symbol.clone(), Value::Integer(i)], env)?;

            if let Value::Return(ret) = eval_expression(value, env)? {
                return Ok(ret.as_ref().clone());
            }
        }
    }

    Ok(Value::Nil)
}

pub fn builtin_dolist(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("DOLIST", 1, args, ArgComp::Min)?;

    let env = &mut Env::extend(env);

    let Value::List(List(header)) = &args[0] else {
        return Err(EvalError::Other(format!(
            "DOLIST header must be a sequence: {}",
            &args[0]
        )));
    };

    let ls = eval_expression(&header[1], env)?;

    match &[&header[0], &ls] {
        [Value::Symbol(s), Value::List(List(l))] => {
            for itm in l {
                for value in &args[1..] {
                    env.set(s, itm.clone());

                    if let Value::Return(ret) = eval_expression(value, env)? {
                        return Ok(ret.as_ref().clone());
                    }
                }
            }

            Ok(Value::Nil)
        }
        _ => Err(EvalError::Other(format!(
            "Invalid DOLIST header: {}",
            List(header.clone())
        ))),
    }
}

pub fn builtin_do(args: &[Value], env: &mut Env) -> ValRes {
    assert_args("DO", 2, args, ArgComp::Min)?;
    assert_args("DO", 3, args, ArgComp::Max)?;

    let mut vars_and_ops = vec![];
    let env = &mut Env::extend(env);

    if let Value::List(List(l)) = &args[0] {
        for list in l {
            if let Value::List(List(list)) = list {
                if list.len() != 3 {
                    return Err(EvalError::Other(format!(
                        "Invalid DO var header {}",
                        args[0]
                    )));
                }

                if let [symbol, value, operation] = list.as_slice() {
                    builtin_setf(&[symbol.clone(), value.clone()], env)?;

                    vars_and_ops.push((symbol, operation));
                }
            } else {
                return Err(EvalError::Other(format!(
                    "Invalid DO var header {}",
                    args[0]
                )));
            }
        }
    }

    let (test, result) = match &args[1] {
        Value::List(List(l)) => (l[0].clone(), l[1].clone()),
        _ => wrong_type!(args[1].to_string(), Type::List),
    };

    loop {
        if args.len() == 3 {
            eval_expression(&args[2], env)?;
        }

        if !eval_expression(&test, env)?.is_null() {
            let res = eval_expression(&result, env)?;

            return Ok(match res {
                Value::Return(v) => v.as_ref().clone(),
                _ => res,
            });
        };

        for (symbol, value) in vars_and_ops.clone() {
            builtin_setf(&[symbol.clone(), value.clone()], env)?;
        }
    }
}
