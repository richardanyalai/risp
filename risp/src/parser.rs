use std::rc::Rc;

use crate::{
    lexer::*,
    types::{Char, Float, Int},
};

#[derive(Debug, thiserror::Error)]
pub enum ParsingError {
    #[error("PARSER: cannot create expression from token {0}")]
    NoSuchExpression(String),

    #[error("PARSER: expected \"ParenOpen\" or \"Quote\", found {0}")]
    InvalidStart(String),

    #[error("PARSER: missing expression after quote")]
    NoExpressionAfterQuote,

    #[error("PARSER: out of tokens")]
    OutOfTokens,

    #[error(transparent)]
    LexingError(#[from] LexingError),
}

#[derive(Clone, Debug, PartialEq)]
pub enum Literal {
    Nil,
    True,
    Integer(Int),
    Float(Float),
    Char(Char),
    String(String),
    Symbol(String),
    Quoted(Rc<SExpression>),
}

#[derive(Clone, Debug, PartialEq)]
pub enum SExpression {
    Literal(Literal),
    List(Vec<SExpression>),
}

impl TryFrom<Token> for Literal {
    type Error = ParsingError;

    fn try_from(token: Token) -> Result<Self, Self::Error> {
        let value = match token {
            Token::Binary(n) | Token::Octal(n) | Token::Decimal(n) | Token::Hexadecimal(n) => {
                Literal::Integer(n)
            }
            Token::Float(n) => Literal::Float(n),
            Token::Char(c) => Literal::Char(c),
            Token::String(s) => Literal::String(s),
            Token::Symbol(s) => match s.to_lowercase().as_str() {
                "nil" => Literal::Nil,
                "t" => Literal::True,
                _ => Literal::Symbol(s),
            },
            _ => return Err(ParsingError::NoSuchExpression(token.to_string())),
        };

        Ok(value)
    }
}

pub fn parse(program: &str) -> Result<SExpression, ParsingError> {
    let mut tokens = tokenize(program)?.into_iter().rev().collect::<Vec<_>>();

    tokens.retain(|token| *token != Token::Comment);

    parse_list(&mut tokens)
}

pub fn parse_list(tokens: &mut Vec<Token>) -> Result<SExpression, ParsingError> {
    let Some(token) = tokens.pop() else {
        return Ok(SExpression::Literal(Literal::Nil));
    };

    if tokens.is_empty() || token == Token::Quote {
        return match token {
            Token::Quote => {
                if tokens.is_empty() {
                    return Err(ParsingError::NoExpressionAfterQuote);
                }

                let sub_list = parse_list(tokens)?;

                let sub_list_new = if let SExpression::List(l) = &sub_list {
                    if l.is_empty() {
                        SExpression::Literal(Literal::Nil)
                    } else {
                        sub_list
                    }
                } else {
                    sub_list
                };

                Ok(SExpression::Literal(Literal::Quoted(Rc::new(sub_list_new))))
            }
            Token::ParenOpen | Token::ParenClose => {
                Err(ParsingError::NoSuchExpression(token.to_string()))
            }
            _ => Ok(SExpression::Literal(Literal::try_from(token)?)),
        };
    }

    if ![Token::ParenOpen, Token::Quote].contains(&token) {
        return Err(ParsingError::InvalidStart(token.to_string()));
    }

    let mut list: Vec<SExpression> = Vec::new();

    while !tokens.is_empty() {
        let Some(token) = tokens.pop() else {
            return Err(ParsingError::OutOfTokens);
        };

        match &token {
            Token::ParenOpen => {
                tokens.push(Token::ParenOpen);

                let sub_list = parse_list(tokens)?;

                list.push(sub_list);
            }
            Token::ParenClose => return Ok(SExpression::List(list)),
            Token::Quote => {
                let token = tokens.pop().ok_or(ParsingError::OutOfTokens)?;

                if token == Token::ParenClose {
                    return Err(ParsingError::NoExpressionAfterQuote);
                }

                if token == Token::ParenOpen {
                    tokens.push(token);

                    let sub_list = parse_list(tokens)?;

                    let sub_list_new = if let SExpression::List(l) = &sub_list {
                        if l.is_empty() {
                            SExpression::Literal(Literal::Nil)
                        } else {
                            sub_list
                        }
                    } else {
                        sub_list
                    };

                    list.push(SExpression::Literal(Literal::Quoted(Rc::new(sub_list_new))));
                } else {
                    list.push(SExpression::Literal(Literal::Quoted(Rc::new(
                        SExpression::Literal(Literal::try_from(token)?),
                    ))));
                }
            }
            _ => list.push(SExpression::Literal(Literal::try_from(token)?)),
        }
    }

    Ok(SExpression::List(list))
}

#[cfg(test)]
mod tests {
    use std::f64::consts::PI;

    use super::*;

    #[test]
    fn test_empty() {
        let list = parse("").unwrap();
        assert_eq!(list, SExpression::Literal(Literal::Nil));

        let list = parse("()").unwrap();
        assert_eq!(list, SExpression::List(vec![]));
    }

    #[test]
    fn test_string() {
        let list = parse("(print \"()\")").unwrap();

        assert_eq!(
            list,
            SExpression::List(vec![
                SExpression::Literal(Literal::Symbol("print".to_string())),
                SExpression::Literal(Literal::String("()".to_string())),
            ])
        );
    }

    #[test]
    fn test_add_two() {
        let list = parse("(+ 1 2)").unwrap();

        assert_eq!(
            list,
            SExpression::List(vec![
                SExpression::Literal(Literal::Symbol("+".to_string())),
                SExpression::Literal(Literal::Integer(1)),
                SExpression::Literal(Literal::Integer(2)),
            ])
        );
    }

    #[test]
    fn test_area_of_a_circle() {
        let program = format!(
            "
              (progn
                (setq r 6.9)
                (setq pi {PI})
                (* pi (* r r)))
            "
        );

        let list = parse(&program).unwrap();

        assert_eq!(
            list,
            SExpression::List(vec![
                SExpression::Literal(Literal::Symbol("progn".to_string())),
                SExpression::List(vec![
                    SExpression::Literal(Literal::Symbol("setq".to_string())),
                    SExpression::Literal(Literal::Symbol("r".to_string())),
                    SExpression::Literal(Literal::Float(6.9)),
                ]),
                SExpression::List(vec![
                    SExpression::Literal(Literal::Symbol("setq".to_string())),
                    SExpression::Literal(Literal::Symbol("pi".to_string())),
                    SExpression::Literal(Literal::Float(PI)),
                ]),
                SExpression::List(vec![
                    SExpression::Literal(Literal::Symbol("*".to_string())),
                    SExpression::Literal(Literal::Symbol("pi".to_string())),
                    SExpression::List(vec![
                        SExpression::Literal(Literal::Symbol("*".to_string())),
                        SExpression::Literal(Literal::Symbol("r".to_string())),
                        SExpression::Literal(Literal::Symbol("r".to_string())),
                    ]),
                ]),
            ])
        );
    }
}
