#![deny(clippy::all)]
#![deny(clippy::dbg_macro)]
#![deny(clippy::redundant_clone)]
#![deny(clippy::clone_on_ref_ptr)]
#![deny(clippy::cargo)]
#![allow(clippy::multiple_crate_versions)]

mod functions;
mod parser;
mod types;

pub mod env;
pub mod eval;
pub mod lexer;
