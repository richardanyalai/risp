use logos::Logos;

use crate::types::{Char, Float, Int};

#[derive(Clone, Debug, Default, PartialEq, thiserror::Error)]
pub enum LexingError {
    #[error("LEXER: mismatching parentheses count")]
    ParenCountMismatch,

    #[error("LEXER: {0}")]
    ParseIntError(#[from] std::num::ParseIntError),

    #[error("LEXER: {0}")]
    ParseFloatError(#[from] std::num::ParseFloatError),

    #[error("LEXER: there is no character with name {0}")]
    InvalidCharacter(String),

    #[default]
    #[error("LEXER: invalid token")]
    Other,
}

#[derive(Debug, derive_more::Display, Logos, PartialEq)]
#[logos(error = LexingError)]
#[logos(skip r"[ \t\n\f]+")]
pub enum Token {
    #[regex(";([^;\n]*)", logos::skip)]
    Comment,

    #[token("(")]
    ParenOpen,

    #[token(")")]
    ParenClose,

    #[regex("'")]
    Quote,

    #[regex(
        "#(b|B)[01]+",
        |lex| Int::from_str_radix(&lex.slice()[2..], 2)
    )]
    Binary(Int),

    #[regex(
        "#(o|O)[0-7]+",
        |lex| Int::from_str_radix(&lex.slice()[2..], 8)
    )]
    Octal(Int),

    #[regex(
        "-?[0-9]+",
        |lex| Int::from_str_radix(lex.slice(), 10),
        priority = 2
    )]
    Decimal(Int),

    #[regex(
        "#(x|X)[0-9a-fA-F]+",
        |lex| Int::from_str_radix(&lex.slice()[2..], 16)
    )]
    Hexadecimal(Int),

    #[regex(
        r"[0-9]*\.[0-9]+([eE][+-]?[0-9]+)?|[0-9]+[eE][+-]?[0-9]+",
        |lex| lex.slice().parse()
    )]
    Float(Float),

    #[regex(
        r"#\\([A-Za-z]+|\d|[A-Za-z\d\s\t\n ])",
        |lex| parse_char(lex.slice().trim_start_matches("#\\"))
    )]
    Char(Char),

    #[regex(
        r#""([^"\\]|\\t|\\u|\\n|\\")*""#,
        |lex| lex.slice()
            .trim_start_matches('\"')
            .trim_end_matches('\"')
            .to_string()
    )]
    String(String),

    #[regex(
        r#"[\w+\-*/=<>_\?:\p{Emoji_Presentation}]+"#,
        |lex| lex.slice().to_string()
    )]
    Symbol(String),
}

fn parse_char(name: &str) -> Result<Char, LexingError> {
    let c = match name.to_uppercase().as_str() {
        "NULL" => Char::Null,
        "BACKSPACE" => Char::Backspace,
        "TAB" | "\t" => Char::Tab,
        "NEWLINE" | "\n" => Char::Newline,
        "LINEFEED" => Char::Linefeed,
        "PAGE" => Char::Page,
        "RETURN" | "\r" => Char::Return,
        "SPACE" | " " => Char::Space,
        "RUBOUT" => Char::Rubout,
        _ => {
            if name.len() == 1 {
                Char::Other(name.chars().last().unwrap_or(0 as char))
            } else {
                return Err(LexingError::InvalidCharacter(name.to_string()));
            }
        }
    };

    Ok(c)
}

pub fn tokenize(input: &str) -> Result<Vec<Token>, LexingError> {
    let lex = Token::lexer(input);
    let mut tokens = vec![];
    let (mut open, mut close) = (0, 0);

    for token in lex {
        match token {
            Ok(Token::ParenOpen) => open += 1,
            Ok(Token::ParenClose) => close += 1,
            _ => {}
        };

        tokens.push(token?);
    }

    if open != close {
        return Err(LexingError::ParenCountMismatch);
    }

    Ok(tokens)
}

#[cfg(test)]
mod test {
    use std::f64::consts::PI;

    use super::*;

    #[test]
    fn test_empty() {
        let tokens = tokenize("").unwrap();
        assert_eq!(tokens, vec![]);
    }

    #[test]
    fn test_char() {
        let tokens = tokenize("#\\a").unwrap();
        assert_eq!(tokens, vec![Token::Char(Char::Other('a'))]);

        let tokens = tokenize("#\\ ").unwrap();
        assert_eq!(tokens, vec![Token::Char(Char::Space)]);

        let tokens = tokenize("#\\a ").unwrap();
        assert_eq!(tokens, vec![Token::Char(Char::Other('a'))]);

        let tokens = tokenize("#\\Return").unwrap();
        assert_eq!(tokens, vec![Token::Char(Char::Return)]);

        let tokens = tokenize("#\\\t").unwrap();
        assert_eq!(tokens, vec![Token::Char(Char::Tab)]);

        let tokens = tokenize(
            "#\\
        ",
        )
        .unwrap();
        assert_eq!(tokens, vec![Token::Char(Char::Newline)]);

        let tokens = tokenize("(#\\a)").unwrap();
        assert_eq!(
            tokens,
            vec![
                Token::ParenOpen,
                Token::Char(Char::Other('a')),
                Token::ParenClose
            ]
        );
    }

    #[test]
    fn test_number() {
        let tokens = tokenize("2.3").unwrap();
        assert_eq!(tokens, vec![Token::Float(2.3)]);

        let tokens = tokenize("#b1000101").unwrap();
        assert_eq!(tokens, vec![Token::Binary(69)]);

        let tokens = tokenize("#o105").unwrap();
        assert_eq!(tokens, vec![Token::Octal(69)]);

        let tokens = tokenize("69").unwrap();
        assert_eq!(tokens, vec![Token::Decimal(69)]);

        let tokens = tokenize("#x45").unwrap();
        assert_eq!(tokens, vec![Token::Hexadecimal(69)]);
    }

    #[test]
    #[should_panic]
    fn test_paren_count_mismatch() {
        let err = Err(LexingError::ParenCountMismatch);

        assert_eq!(tokenize("("), err);
        assert_eq!(tokenize(")"), err);
        assert_eq!(tokenize("(#\\)"), err);
    }

    #[test]
    fn test_comment() {
        let program = "
            ; this program does nothing
            ; these comments are redundant as hell
            ()
        ";

        let tokens = tokenize(program).unwrap();

        assert_eq!(tokens, vec![Token::ParenOpen, Token::ParenClose]);
    }

    #[test]
    fn test_quoted() {
        let tokens = tokenize("'(1 2 '(3 4))").unwrap();

        assert_eq!(
            tokens,
            vec![
                Token::Quote,
                Token::ParenOpen,
                Token::Decimal(1),
                Token::Decimal(2),
                Token::Quote,
                Token::ParenOpen,
                Token::Decimal(3),
                Token::Decimal(4),
                Token::ParenClose,
                Token::ParenClose,
            ]
        );
    }

    #[test]
    fn test_print_string() {
        let tokens = tokenize("(print \"()\")").unwrap();

        assert_eq!(
            tokens,
            vec![
                Token::ParenOpen,
                Token::Symbol("print".to_string()),
                Token::String("()".to_string()),
                Token::ParenClose
            ]
        );
    }

    #[test]
    #[should_panic]
    fn test_print_string_missing_closing_quote() {
        tokenize("(print \"Hello)").unwrap();
    }

    #[test]
    fn test_add_two() {
        let tokens = tokenize("(+ 1 2)").unwrap();

        assert_eq!(
            tokens,
            vec![
                Token::ParenOpen,
                Token::Symbol("+".to_string()),
                Token::Decimal(1),
                Token::Decimal(2),
                Token::ParenClose,
            ]
        );
    }

    #[test]
    fn test_area_of_a_circle() {
        let program = format!(
            "
              (progn
                (setq r 6.9)
                (setq pi {PI})
                (* pi (* r r)))
            "
        );

        let tokens = tokenize(&program).unwrap();

        assert_eq!(
            tokens,
            vec![
                Token::ParenOpen,
                Token::Symbol("progn".to_string()),
                Token::ParenOpen,
                Token::Symbol("setq".to_string()),
                Token::Symbol("r".to_string()),
                Token::Float(6.9),
                Token::ParenClose,
                Token::ParenOpen,
                Token::Symbol("setq".to_string()),
                Token::Symbol("pi".to_string()),
                Token::Float(PI),
                Token::ParenClose,
                Token::ParenOpen,
                Token::Symbol("*".to_string()),
                Token::Symbol("pi".to_string()),
                Token::ParenOpen,
                Token::Symbol("*".to_string()),
                Token::Symbol("r".to_string()),
                Token::Symbol("r".to_string()),
                Token::ParenClose,
                Token::ParenClose,
                Token::ParenClose
            ]
        );
    }
}
