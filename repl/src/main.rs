#![deny(clippy::all)]
#![deny(clippy::dbg_macro)]
#![deny(clippy::redundant_clone)]
#![deny(clippy::clone_on_ref_ptr)]
#![deny(clippy::cargo)]

use std::sync::Arc;

use clap::Parser;
use linefeed::{
    complete::PathCompleter, Command, Function, Interface, Prompter, ReadResult, Terminal,
};

use risp::{
    env::Env,
    eval,
    lexer::{tokenize, LexingError},
};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Specify lisp file to be loaded
    #[arg(short, long)]
    input: Option<String>,
}

const BLINKING_BAR: &str = "\x1b[\x31 q";
const COLOR_GREEN: &str = "\x1b[32m";
const COLOR_RED: &str = "\x1b[31m";
const COLOR_RESET: &str = "\x1b[97m";

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut env = Env::default();
    let args = Args::parse();

    if let Some(input) = args.input {
        let res = eval::eval(&format!("(load \"{input}\")"), &mut env)?;

        println!("{res}");

        return Ok(());
    }

    let reader = Interface::new("risp")?;

    reader.define_function("enter-function", Arc::new(EnterFunction));
    reader.bind_sequence("\r", Command::from_str("enter-function"));
    reader.bind_sequence("\n", Command::from_str("enter-function"));
    reader.set_completer(Arc::new(PathCompleter));
    reader.set_prompt(&format!(
        "リスプ-{}> {BLINKING_BAR}",
        env!("CARGO_PKG_VERSION")
    ))?;

    while let ReadResult::Input(input) = reader.read_line()? {
        match eval::eval(input.as_ref(), &mut env) {
            Ok(val) => println!("{COLOR_GREEN}{val}{COLOR_RESET}"),
            Err(e) => println!("{COLOR_RED}*** - {e}{COLOR_RESET}"),
        }

        if !input.trim().is_empty() {
            reader.add_history_unique(input);
        }
    }

    println!("Goodbye, Friend");

    Ok(())
}

struct EnterFunction;

impl<Term: Terminal> Function<Term> for EnterFunction {
    fn execute(&self, prompter: &mut Prompter<Term>, count: i32, _: char) -> std::io::Result<()> {
        if tokenize(prompter.buffer()) != Err(LexingError::ParenCountMismatch) {
            prompter.accept_input()
        } else if count > 0 {
            prompter.insert(count as usize, '\n')
        } else {
            Ok(())
        }
    }
}
